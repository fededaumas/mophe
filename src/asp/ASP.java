package asp;

import he.BaseHistogramEqualizer;
import hsig.SigmoidHistogramEqualizer;
import ij.plugin.histogram2.HistogramMatcher;
import ij.process.ByteProcessor;

import java.io.BufferedWriter;
import java.io.FileWriter;

import utils.Imagen;
import utils.ProblemUtils;

public class ASP {
	
	private Imagen imagenOriginal;
	private Imagen imagenNueva;
	private String path;
	
	private BaseHistogramEqualizer baseHistogramEqualizer;
	private SigmoidHistogramEqualizer sigHistogramEqualizer;
	
	
	public ASP (String filePath){
		this.imagenOriginal = new Imagen(filePath);
		this.baseHistogramEqualizer = new BaseHistogramEqualizer(new Imagen(filePath));
		this.sigHistogramEqualizer = new SigmoidHistogramEqualizer(new Imagen(filePath));
	}
	
	public ASP(Imagen imagenOriginal){
		this.imagenOriginal = imagenOriginal;
		this.baseHistogramEqualizer = new BaseHistogramEqualizer(new Imagen(imagenOriginal.getSrcImage()));
		this.sigHistogramEqualizer = new SigmoidHistogramEqualizer(new Imagen(imagenOriginal.getSrcImage()));
	}
	
	
	/**
	 * Aplicar paper formula 
	 * 
	 * h = (hi + øheq + µhsig) / (1 + ø + µ )
	 */
	
	
	public void equalize(double ø , double µ){
		int[] h = new int[256]; // h
		Imagen imagenOriginal = new Imagen(this.imagenOriginal.getSrcImage());
		ByteProcessor imagenOriginalJ = (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
		ByteProcessor imagenOrig2= (ByteProcessor) new ByteProcessor(imagenOriginal.getSrcImage()).duplicate();
		imagenOriginalJ.resetRoi();
		int[] hi = imagenOriginalJ.getHistogram(); // hi
		this.baseHistogramEqualizer.equalize();
		int[] heq = baseHistogramEqualizer.getImagenNueva().getHistograma(); //heq
		this.sigHistogramEqualizer.equalize();
		int[] hsig = this.sigHistogramEqualizer.getImagenNueva().getHistograma(); //hsig
		
		for (int i = 0 ; i<256; i++){
			h[i] = (int) (Math.round(hi[i] + (ø * heq[i]) + (µ * hsig[i])) / (1 + ø + µ ));
		}
		HistogramMatcher matcher = new HistogramMatcher();
		
		
		imagenOriginalJ.applyTable(matcher.matchHistograms(hi, h));
		this.imagenNueva =  new Imagen(imagenOriginalJ.getBufferedImage());
		
		
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(this.path!=null?this.path + "/ASPFUN.txt" : "rsc/files/ASPFUN.txt", false));
			double entropy = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(),imagenNueva.getCantPixeles());
			double ssim = ProblemUtils.calcularSSIM(imagenOrig2,imagenOriginalJ);
			bw.append(-entropy + " " + -ssim );
			bw.close();
		}catch(Exception e){
			System.out.println(e.getStackTrace());
		}
		
	}
	
	@Deprecated
	public int[] desdeHistograma (int[] h){
		imagenNueva.setHistograma(h);
		int [] t = new int[256];
		
		for (int i =0; i<256; i++){
			t[i] = (int)Math.round(255 * (imagenNueva.getFDA(i)) + 0.5);
		}
		return t;
	}
	
	
	public BaseHistogramEqualizer getBaseHistogramEqualizer() {
		return baseHistogramEqualizer;
	}
	public void setBaseHistogramEqualizer(
			BaseHistogramEqualizer baseHistogramEqualizer) {
		this.baseHistogramEqualizer = baseHistogramEqualizer;
	}
	public SigmoidHistogramEqualizer getSigHistogramEqualizer() {
		return sigHistogramEqualizer;
	}
	public void setSigHistogramEqualizer(
			SigmoidHistogramEqualizer sigHistogramEqualizer) {
		this.sigHistogramEqualizer = sigHistogramEqualizer;
	}
	public Imagen getImagenOriginal() {
		return imagenOriginal;
	}
	public void setImagenOriginal(Imagen imagenOriginal) {
		this.imagenOriginal = imagenOriginal;
	}
	public Imagen getImagenNueva() {
		return imagenNueva;
	}
	public void setImagenNueva(Imagen imagenNueva) {
		this.imagenNueva = imagenNueva;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

}
