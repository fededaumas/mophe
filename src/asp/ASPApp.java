package asp;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import ij.process.ByteProcessor;
import jmetal.util.Configuration;
import utils.GraphicsHelper;
import utils.ProblemUtils;

public class ASPApp {
	
	private ASP asp;
	private String path;

	public static String nombreImagen;
    public static String formatoImagen;
    
    private static Logger logger = Logger.getLogger(ASPApp.class);
	
	static {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("rsc/files/image_file.properties");

			// load a properties file
			prop.load(input);

			nombreImagen = prop.getProperty("nombreimagen");
			formatoImagen = prop.getProperty("formato");
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}	
	
	public ASPApp(){
		String ruta = "rsc/images/baseImages/";
		this.asp = new ASP(ruta + nombreImagen + "." + formatoImagen);
		this.asp.setPath(this.path);
	}
	
	public ASPApp(String path, String nombreImagen, String formato){
		this.path = path;
		String ruta = "rsc/images/baseImages/";
		this.asp = new ASP(ruta + nombreImagen + "." + formato);
		this.asp.setPath(this.path);
	}
	
	public ASPApp( String nombreImagen, String formato){
		String ruta = "rsc/images/baseImages/";
		this.asp = new ASP(ruta + nombreImagen + "." + formato);
		this.asp.setPath(this.path);
	}
	

	
	public static void main(String[] args) {
		
		String ruta = "rsc/images/baseImages/";
		
		ASPApp app;
		double [][] aspValues = new double[3][2];
		aspValues[0][0] = 0.001;
		aspValues[0][1] = 0.9611;
		aspValues[1][0] = 0.001;
		aspValues[1][1] = 1.0381;
		aspValues[2][0] = 0.0009;
		aspValues[2][1] = 1.1307;
		
		Double qualityIndex = 0D;
		int bestTitaGamaIndex = 0;
		for (int i=0; i<3; i++){
			app = new ASPApp();
			app.asp.equalize(aspValues[i][0], aspValues[i][1]);
			ByteProcessor imagenOriginalJ = new ByteProcessor(
					app.asp.getImagenOriginal().getWidth(),
					app.asp.getImagenOriginal().getHeight(),
					app.asp.getImagenOriginal().getSrcData());
			ByteProcessor imagenNuevaJ = new ByteProcessor(
					app.asp.getImagenOriginal().getWidth(),
					app.asp.getImagenOriginal().getHeight(),
					app.asp.getImagenNueva().getSrcData());
			Double nqi = ProblemUtils.calcularSSIM(imagenOriginalJ, imagenNuevaJ);
			if (nqi > qualityIndex){
				qualityIndex = nqi;
				bestTitaGamaIndex = i;
			}
		}
		app = new ASPApp();
		app.asp.equalize(aspValues[bestTitaGamaIndex][0], aspValues[bestTitaGamaIndex][1]);
		GraphicsHelper.imprimirFrame(ruta, nombreImagen, app.asp.getImagenOriginal(), app.asp.getImagenNueva(), "ASP" + bestTitaGamaIndex);
		double entropy = ProblemUtils.calcularEntropia(app.asp.getImagenNueva().getHistograma(), app.asp.getImagenNueva().getCantPixeles());
		ByteProcessor imagenOriginalJ = new ByteProcessor(
				app.asp.getImagenOriginal().getWidth(),
				app.asp.getImagenOriginal().getHeight(),
				app.asp.getImagenOriginal().getSrcData());
		ByteProcessor imagenNuevaJ = new ByteProcessor(
				app.asp.getImagenOriginal().getWidth(),
				app.asp.getImagenOriginal().getHeight(),
				app.asp.getImagenNueva().getSrcData());
		double ssim = ProblemUtils.calcularSSIM(imagenOriginalJ, imagenNuevaJ);
		
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter("rsc/files/ASPFUN.txt", false));
			bw.append(entropy + " " + ssim);
			bw.close();
		} catch (IOException e) {
			Configuration.logger_.severe("Error acceding to the file");
			e.printStackTrace();
		}
		
		
	}
	
	
	public ASP getAsp() {
		return asp;
	}
	
	public void setAsp(ASP asp) {
		this.asp = asp;
	}

}
