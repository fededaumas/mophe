package utils;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ij.plugin.SSIM_index;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import mohe2.DensidadProperties;

public class ProblemUtils {

	
	public static BigDecimal calcularContraste(int maxLevel, int minLevel) {
		//Michelson contrast
		return new BigDecimal(maxLevel-minLevel).divide(new BigDecimal(maxLevel +minLevel ),20, RoundingMode.HALF_EVEN);
		
	}
	
	public static BigDecimal calcularContraste(BigDecimal promedioBack, BigDecimal promedioFore) {
		//Michelson contrast
		//return new BigDecimal(maxLevel-minLevel).divide(new BigDecimal(maxLevel +minLevel ),20, RoundingMode.HALF_EVEN);
		BigDecimal contraste = BigDecimal.ZERO;
		contraste = (promedioFore.subtract(promedioBack)).divide((promedioFore.add(promedioBack)),20, RoundingMode.HALF_EVEN);
		
		return contraste.abs();
		//return new BigDecimal(promedioFore-promedioBack).divide(new BigDecimal(promedioFore +promedioBack),20, RoundingMode.HALF_EVEN);
	}

	public static BigDecimal calcularPromedio(int[] histograma, int threshold, int modo) {
		
		BigDecimal promedio = BigDecimal.ZERO;
		double suma = 0;
		int cant=0;
		
		if (modo==0){
			for (int i = 0; i <= threshold; i++){
				suma += i* histograma[i];
				cant += histograma[i];
			}
			promedio = new BigDecimal(suma/cant);
		}else if (modo==1){
			cant=0;
			for (int i = threshold+1 ; i < 256 ; i++){
				suma += i* histograma[i];
				cant += histograma[i];
			}
			promedio = new BigDecimal(suma/cant);
		}else{
			for (int i = 0; i < 256; i++){
				suma += i* histograma[i];
				cant += histograma[i];
			}
			promedio = new BigDecimal(suma/cant);
		}
		return promedio;
	}
	
	public static double calcularContraste2(Imagen imagen){
		double suma =0 ;
		double luminancia = imagen.getMedia();
		for (int i=0; i<imagen.getHeight();i++){
			for (int j=0; j<imagen.getWidth();j++){
				suma+= Math.pow((imagen.getRaster().getSample(j, i, 0) - luminancia),2);
			}
		}
		
		return Math.sqrt(suma/((imagen.getHeight()*imagen.getWidth())));
	}
	
	public static byte[] obtenerByte(BufferedImage nuevaImagen) {
		// Get raw image data
		Raster raster = nuevaImagen.getData();
		DataBuffer buffer = raster.getDataBuffer();
		int type = buffer.getDataType();

		if (type != DataBuffer.TYPE_BYTE) {
			System.err.println("Wrong image data type");
			System.exit(1);
		}
		if (buffer.getNumBanks() != 1) {
			System.err.println("Wrong image data format");
			System.exit(1);
		}

		DataBufferByte byteBuffer = (DataBufferByte) buffer;
		byte[] srcData = byteBuffer.getData(0);

		return srcData;
	}

	public static int[] calcularHistograma(byte[] srcData) {

		int ptr;
		int histograma[] = new int[256];
		// Clear histogram data
		// Set all values to zero
		ptr = 0;
		while (ptr < histograma.length)
			histograma[ptr++] = 0;

		// Calculate histogram and find the level with the max value
		// Note: the max level value isn't required by the Otsu method
		ptr = 0;

		while (ptr < srcData.length) {
			int h = 0xFF & srcData[ptr];
			histograma[h]++;
			ptr++;
		}
		return histograma;
	}

	public static double calcularEntropia(int[] histograma, int totalPixeles) {
		double entropia = 0;
		for (int i = 0; i < 256; i++) {
			double probabilidad = (double) histograma[i] / totalPixeles;
			if (probabilidad == 0)
				entropia += 0;
			else
				entropia += -(probabilidad * (Math.log(probabilidad) / Math
						.log(2)));
		}
		return entropia;
	}
	
	
	public static double calcularSSIM(ImageProcessor imagenOriginal, ImageProcessor imagenNueva) {
		SSIM_index index = new SSIM_index(imagenOriginal,imagenNueva);
		return index.getSSIMIndex();
	}
	
	public static double calcularSSIM(Imagen imagenO, Imagen imagenN) {
		ByteProcessor imagenOriginal = (ByteProcessor) new ByteProcessor(imagenO.getSrcImage()).duplicate();
		ByteProcessor imagenNueva = (ByteProcessor) new ByteProcessor(imagenN.getSrcImage()).duplicate();
		SSIM_index index = new SSIM_index(imagenOriginal,imagenNueva);
		return index.getSSIMIndex();
	}

	public static DensidadProperties getDensidadProperties(int[] h) {
		DensidadProperties dp = new DensidadProperties();
		int K = h.length;
		int n = 0;			// sum all histogram values
		double maxDensity = 0D;
		double promedioDensidad = 0D;
		double sumaDensidad =0D;
		for (int i=0; i<K; i++)	{
			n += h[i];
		}
		double[] p = new double[K];
		for (int i=0; i<h.length; i++) {
			p[i] =  (double) h[i] / n;
			maxDensity = maxDensity<p[i]?p[i]:maxDensity;
			sumaDensidad += p[i];
			promedioDensidad += p[i]/K; 
		}
		
		dp.setHistDensidad(p);
		dp.setMaxDensidad(maxDensity);
		dp.setSumDensidad(sumaDensidad);
		dp.setPromedioDensidad(promedioDensidad);
		return dp;
	}
	
	public static double calcularMSE(Imagen o, Imagen n){
		double mse = 0;
		for (int i =0; i<o.getWidth(); i++){
			for (int j =0; j<o.getHeight(); j++){
				mse += Math.pow((o.getMatrizOriginal()[i][j] - n.getMatrizOriginal()[i][j]),2);
			}
		}
		mse = mse/ o.getCantPixeles();
		return mse;
	}
	
	public static double calcularPSNR(Imagen o, Imagen n){
		double mse = calcularMSE(o, n);
		return 10 * Math.log10(255*255/mse);
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
}
