package utils;

public class PropiedadesImagen {
	
	public static void main(String[] args) {

		String imagePath = args[0];
		Imagen imagen = new Imagen(imagePath);
		if(args.length>1){
			String imageOPath = args[1];
			Imagen imagenO = new Imagen(imageOPath);
			System.out.println("SSIM: " +ProblemUtils.calcularSSIM(imagenO, imagen));
			System.out.println("MSE: " +ProblemUtils.calcularMSE(imagenO, imagen));
			System.out.println("PSNR: " +ProblemUtils.calcularPSNR(imagenO, imagen));
		}
		
		System.out.println("Entropía: " +ProblemUtils.calcularEntropia(imagen.getHistograma(), imagen.getCantPixeles()));
		System.out.println("Desviación Standard: " +ProblemUtils.calcularContraste2(imagen));
		System.out.println("Brillo Medio (Luminacia): " +imagen.getMedia());
		
		
		
	}

}
