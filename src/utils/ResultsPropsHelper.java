package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ResultsPropsHelper {
	
	private double promedioSSIM;
	private double promedioContraste;
	private double promedioEntropia;
	
	private double desviacionSSIM;
	private double desviacionContraste;
	private double desviacionEntropia;
	public enum Method{
		STANDARIZACION, CLUSTERIZACION_SSIM;
	}
	
	private List<Imagen> resultados = new ArrayList<Imagen>();
	
	public ResultsPropsHelper(){
		
	}
	
	private List<Imagen> standarizacionMethod(Imagen imagenOriginal, List<Imagen> imagenes, Integer nroSoluciones){
		List<Imagen> resultadosPorEstandarizacion = new ArrayList<Imagen>();
		double potenciaContraste = 0;
		double potenciaSSIM = 0;
		double potenciaEntropia = 0;
		int i=0;
		for (Imagen imagenNueva : imagenes){
			double contraste = ProblemUtils.calcularContraste2(imagenNueva);
			double ssim = ProblemUtils.calcularSSIM(imagenOriginal, imagenNueva);
			double entropia = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(), imagenNueva.getCantPixeles());
			promedioSSIM += ssim;
			promedioEntropia +=entropia;
			promedioContraste +=contraste;
			
			potenciaContraste += Math.pow(promedioContraste, 2);
		    desviacionContraste = Math.sqrt(i*potenciaContraste - Math.pow(promedioContraste, 2))/i;
		    
		    potenciaSSIM += Math.pow(promedioSSIM, 2);
		    desviacionSSIM = Math.sqrt(i*potenciaSSIM - Math.pow(promedioSSIM, 2))/i;
		    
		    potenciaEntropia += Math.pow(promedioEntropia, 2);
		    desviacionEntropia = Math.sqrt(i*potenciaEntropia - Math.pow(promedioEntropia, 2))/i;
			i++;
		    
		}
		promedioSSIM = promedioSSIM / imagenes.size();
		promedioContraste = promedioContraste / imagenes.size();
		promedioEntropia = promedioEntropia / imagenes.size();
		
		Map<Imagen, Double> sumasEstandarizadas = new LinkedHashMap<Imagen,Double>();
		for (Imagen imagenNueva : imagenes){
			double contraste = ProblemUtils.calcularContraste2(imagenNueva);
			double ssim = ProblemUtils.calcularSSIM(imagenOriginal, imagenNueva);
			double entropia = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(), imagenNueva.getCantPixeles());
			double sSSIM = (ssim - promedioSSIM) / desviacionSSIM;
			double sContraste = (contraste - promedioContraste) / desviacionContraste;
			double sEntropia = (entropia - promedioEntropia) / desviacionEntropia;
			
			sumasEstandarizadas.put(imagenNueva, (sSSIM + sContraste + sEntropia));
		}
		sumasEstandarizadas = ProblemUtils.sortByValue(sumasEstandarizadas);
		
		List<Imagen> mapKeys = new ArrayList<>(sumasEstandarizadas.keySet());
		for (int j=0 ; j<nroSoluciones ; j++){
			resultadosPorEstandarizacion.add(mapKeys.get(j));
		}
		return resultadosPorEstandarizacion;
	}
	
	private List<Imagen> clusterizationMethod(Imagen imagenOriginal, List<Imagen> imagenes, Integer nroSoluciones) {
		List<Imagen> resultadosPorClusterizacionEstandarizacion = new ArrayList<Imagen>();
		double minSSIM = 1;
		double maxSSIM = 0;
		double cluster = 0;
		for (Imagen imagenNueva : imagenes){
			double ssim = ProblemUtils.calcularSSIM(imagenOriginal, imagenNueva);
			if (ssim < minSSIM){
				minSSIM = ssim;
			}
			if (ssim > maxSSIM){
				maxSSIM = ssim;
			}
			cluster = (maxSSIM-minSSIM)/nroSoluciones;
		}
		
		Map<Double, List<Imagen>> clusterSSIM = new HashMap<Double,List<Imagen>>();
		
		for (Imagen imagenNueva : imagenes){
			double ssim = ProblemUtils.calcularSSIM(imagenOriginal, imagenNueva);
			
			for (double i = minSSIM; i<maxSSIM; i=i+cluster){
				if (ssim>= i && ssim<=(i+cluster)){
					if (!clusterSSIM.containsKey(i)){
						clusterSSIM.put(i, new ArrayList<Imagen>());
					}
					clusterSSIM.get(i).add(imagenNueva);
				}
			}
			
		}
		for (List<Imagen> imagenesClustered : clusterSSIM.values()){
			List<Imagen> resultadosClustered = standarizacionMethod(imagenOriginal, imagenesClustered, 1);
			resultadosPorClusterizacionEstandarizacion.addAll(resultadosClustered);
		}
		
		
		return resultadosPorClusterizacionEstandarizacion;
	}
	
	public ResultsPropsHelper(Imagen imagenOriginal, List<Imagen> imagenes, Method method, Integer nroSoluciones){
		switch(method){
			case STANDARIZACION:
				this.resultados = standarizacionMethod(imagenOriginal, imagenes, nroSoluciones);
				break;
			case CLUSTERIZACION_SSIM:
			default:
				this.resultados = clusterizationMethod(imagenOriginal, imagenes, nroSoluciones);
				break;
		}
		
		
	}

	

	public double getPromedioSSIM() {
		return promedioSSIM;
	}

	public void setPromedioSSIM(double promedioSSIM) {
		this.promedioSSIM = promedioSSIM;
	}

	public double getPromedioContraste() {
		return promedioContraste;
	}

	public void setPromedioContraste(double promedioContraste) {
		this.promedioContraste = promedioContraste;
	}

	public double getPromedioEntropia() {
		return promedioEntropia;
	}

	public void setPromedioEntropia(double promedioEntropia) {
		this.promedioEntropia = promedioEntropia;
	}

	public double getDesviacionSSIM() {
		return desviacionSSIM;
	}

	public void setDesviacionSSIM(double desviacionSSIM) {
		this.desviacionSSIM = desviacionSSIM;
	}

	public double getDesviacionContraste() {
		return desviacionContraste;
	}

	public void setDesviacionContraste(double desviacionContraste) {
		this.desviacionContraste = desviacionContraste;
	}

	public double getDesviacionEntropia() {
		return desviacionEntropia;
	}

	public void setDesviacionEntropia(double desviacionEntropia) {
		this.desviacionEntropia = desviacionEntropia;
	}

	public List<Imagen> getResultadosPorEstandarizacion() {
		return resultados;
	}

	public void setResultadosPorEstandarizacion(List<Imagen> resultadosPorEstandarizacion) {
		this.resultados = resultadosPorEstandarizacion;
	}
	
	
}
