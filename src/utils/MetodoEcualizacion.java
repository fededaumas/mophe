/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.List;

import ij.process.ImageProcessor;

/**
 * @author Pabla Aquino
 * @author Freddy Lugo
 */
public class MetodoEcualizacion {
    
    //Constructor por defecto de la clase
    public MetodoEcualizacion(){
    }
    
    public static ImageProcessor ecualizacionGlobal(ImageProcessor ip, int [] histogramaTotal){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        //Obtenemos la funcion f(x) o funcion de ecualizacion
        Double [] funcionEcualizacion = obtenerFuncionDeEcualizacion(histogramaTotal, 0.0, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f(X(i,j)| para todo X(i,j) perteneciente a X)
        */
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                //Se obtiene la intesidad del pixel actual que se usara como indice para la funcion de ecualizacion
                int intensidadPixelActual = ip.getPixel(i, j);
                ip.putPixelValue(i, j, funcionEcualizacion[intensidadPixelActual]);
            }
        }
        return ip;
    }
    
    public static ImageProcessor ecualizacionBBHE(ImageProcessor ip, int [] histogramaTotal){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();        
        
        //Numero total de pixeles en la imagen
        double n = altura * ancho;
        
        //Array que contiene la propabilidad de ocurrencia de cada intensidad de la imagen
        Double funcionProbabilidad [] = new Double[histogramaTotal.length] ;
        
        //Obtenemos la funcion de probabilidad
        for(int i = 0; i < histogramaTotal.length; i++){
            funcionProbabilidad[i] = histogramaTotal[i]/n;
        }
        
        //Obtenemos la intensidad media de los pixeles en la imagen
        Double xm = 0.0;
        for(int i = 0; i < funcionProbabilidad.length; i++){
            xm += funcionProbabilidad[i] * i;
        }
        
        /*Xt viene dado por la formula:
            * Xt = Xm
         *donde:
            * Xm: es la intensidad media de los pixeles en la imagen.
        */
        
        Double xt = xm;
        
        /*En base al Xt hallado procedemos a dividir el histograma total en:
            *lowerHistogram: que posee las intensidades de 0 a Xt.
            *upperHistogram: que posee las intensideades de (Xt + 1) a 255.
        */
        int [] lowerHistogram = new int [xt.intValue() + 1];
        
        //Se halla la longitud del upperHistogram y se inicializa el mismo
        int longitudUpperHistogram = 255 - xt.intValue();
        int [] upperHistogram = new int [longitudUpperHistogram];
        
        //Cargamos cada histograma con sus respectivos valores a partir del histograma total
        int contadorAux = -1;
        for(int i = 0; i < histogramaTotal.length; i++){
            if(i <= xt){
                //Si las intensidades van de 0 - xt, se guardan en lowerHistogram
                lowerHistogram[i] = histogramaTotal[i];
            }else{
                //Si van de (xt+1) a 255 se guarda en upperHistogram
                contadorAux ++;
                upperHistogram[contadorAux] = histogramaTotal[i];
            }
        }
        
        //Se obtienen las funciones de ecualizacion tanto para el lowerHistogram como para el upperHistogram
        Double [] funcionEcualizacionLower = obtenerFuncionDeEcualizacion(lowerHistogram, 0.0, xt.intValue());
        
        Double [] funcionEcualizacionUpper = obtenerFuncionDeEcualizacion(upperHistogram, xt.intValue() + 1, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        int indiceInicialUpper = xt.intValue() + 1;
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                int intensidadPixelActual = ip.getPixel(i, j);
                if(intensidadPixelActual < indiceInicialUpper){
                    //Si la intensidad actual se encuentra en la parte baja del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionLower[intensidadPixelActual]);
                }else{
                    //Si se ecuentra en la parte superior del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionUpper[intensidadPixelActual - indiceInicialUpper]);
                }
            }
        }
        return ip;
    }
    
    public static ImageProcessor ecualizacionDSIHE(ImageProcessor ip, int [] histogramaTotal){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        //Array que contiene la propabilidad de ocurrencia de cada intensidad de la imagen
        Double funcionProbabilidad [] = new Double[histogramaTotal.length];
        
        //Numero total de pixeles en la imagen
        double n = altura * ancho;
        
        //Obtenemos la probabilidad de aparicion de cada intensidad 
        for(int i = 0; i < histogramaTotal.length; i++){
            funcionProbabilidad[i] = histogramaTotal[i]/n;
        }
        
        //Xe representa el nivel de gris cuya probabilidad acumulada es de 0.5
        double xe = 0.0;
        
        //Booleano que indica si se econtro el nivel de gris con propabilidad acumulada de 0.5
        boolean seEncontroIntensidad = false;
        
        /*Obtenemos el histograma acumulado normalizado mediante la sumarizacion de las probabilidades
         *obtenidas anteriormente*/
        Double acumuladorAux = 0.0;
        for(int i = 0; i < funcionProbabilidad.length; i++){
            acumuladorAux += funcionProbabilidad[i];
            
            /*Buscamos la intensidad que posea probabilidad de 0.5, o en todo caso su inmediato sucesor
             *a medida que construimos el histograma acumulado*/
            if(!seEncontroIntensidad && acumuladorAux >= 0.5){
                xe = i;
                seEncontroIntensidad = true;
                break;
            }
        }
        
        /*Xt viene dado por la formula:
            * Xt = Xe
         *donde:
            * Xe: representa el nivel de gris cuya probabilidad acumulada es de 0.5
        */
        
        Double xt = xe;
        
        /*En base al Xt hallado procedemos a dividir el histograma total en:
            *lowerHistogram: que posee las intensidades de 0 a Xt -1.
            *upperHistogram: que posee las intensideades de Xt a 255.
        */
        int [] lowerHistogram = new int [xt.intValue()];
        
        //Se halla la longitud del upperHistogram y se inicializa el mismo, se le suma 1 ya que se incluye el extremo xt
        int longitudUpperHistogram = (255 - xt.intValue()) +1;
        int [] upperHistogram = new int [longitudUpperHistogram];
        
        //Cargamos cada histograma con sus respectivos valores a partir del histograma total
        int contadorAux = -1;
        for(int i = 0; i < histogramaTotal.length; i++){
            if(i < xt){
                //Si las intensidades van de 0 - (xt-1), se guardan en lowerHistogram
                lowerHistogram[i] = histogramaTotal[i];
            }else{
                //Si van de xt a 255 se guarda en upperHistogram
                contadorAux ++;
                upperHistogram[contadorAux] = histogramaTotal[i];
            }
        }
        
        //Se obtienen las funciones de ecualizacion tanto para el lowerHistogram como para el upperHistogram
        Double [] funcionEcualizacionLower = obtenerFuncionDeEcualizacion(lowerHistogram, 0.0, xt.intValue()-1);
        
        Double [] funcionEcualizacionUpper = obtenerFuncionDeEcualizacion(upperHistogram, xt.intValue(), 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        int indiceInicialUpper = xt.intValue();
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                int intensidadPixelActual = ip.getPixel(i, j);
                if(intensidadPixelActual < indiceInicialUpper){
                    //Si la intensidad actual se encuentra en la parte baja del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionLower[intensidadPixelActual]);
                }else{
                    //Si se ecuentra en la parte superior del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionUpper[intensidadPixelActual - indiceInicialUpper]);
                }
            }
        }
        
        return ip;
    }
        
    public static ImageProcessor ecualizacionMMBEBHE(ImageProcessor ip, int [] histogramaTotal){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        double n = altura * ancho;
        
        //Variable que alamacenara el minMBE encontrado 
        double minMBE = Double.MAX_VALUE;
        
        //Intenisdades de la imagen original
        double intensidadMediaOriginal = 0.0;
        
        //Acumulador de MBE
        double acMBE = 0.0;
        
        //Punto de division del histograma global
        int xt = 0;
        
        //Funcion de probabilidad de la imagen original
        Double [] funcionProbabilidad = new Double[histogramaTotal.length];
        
        //Obtenemos la funcion de probabilidad de la imagen original
        for(int i = 0; i < histogramaTotal.length; i++){
            funcionProbabilidad[i] = histogramaTotal[i]/n;
        }
        
        //Calculamos la intensidad media de la imagen sin ecualizar
        for(int i = 0; i < funcionProbabilidad.length; i++){
            intensidadMediaOriginal += funcionProbabilidad[i] * i;
        }
        
        /*Procedemos a hallar el menor MBE (Mean Brigthness Error) usando la siguiente ecuacion:
            MBE_0 = 0.5 * (L * (1 - P(X_0))) - E(X) para i = 0
            MBE_i = MBE_i-1 + 0.5 * (1 - (L * P(X_i))) para i = 1 hasta 255,
            donde L = 256 ya que se trata de una imagen de escala de grises de 8-bit
            
        */
        for(int i = 0; i < histogramaTotal.length; i++){
            if(i == 0){
                acMBE = (0.5 * (256 * (1 - funcionProbabilidad[i]))) - intensidadMediaOriginal;
            }else{
                acMBE = acMBE + (0.5 * (1 - (256 * funcionProbabilidad[i]))); 
            }
            
            //Si acMBE es menor al menor MBE encontrado hasta ahora entonces guardamos el valor y el punto de corte
            if(Math.abs(acMBE) < minMBE){
                minMBE = Math.abs(acMBE);
                xt = i;
            }
        }
        
        /*En base al Xt hallado procedemos a dividir el histograma total en:
            *lowerHistogram: que posee las intensidades de 0 a Xt.
            *upperHistogram: que posee las intensideades de (Xt + 1) a 255.
        */
        int [] lowerHistogram = new int [xt + 1];
        
        //Se halla la longitud del upperHistogram y se inicializa el mismo
        int longitudUpperHistogram = 255 - xt;
        int [] upperHistogram = new int [longitudUpperHistogram];
        
        //Cargamos cada histograma con sus respectivos valores a partir del histograma total
        int contadorAux = -1;
        for(int i = 0; i < histogramaTotal.length; i++){
            if(i <= xt){
                //Si las intensidades van de 0 - xt, se guardan en lowerHistogram
                lowerHistogram[i] = histogramaTotal[i];
            }else{
                //Si van de (xt+1) a 255 se guarda en upperHistogram
                contadorAux ++;
                upperHistogram[contadorAux] = histogramaTotal[i];
            }
        }
        
        //Se obtienen las funciones de ecualizacion tanto para el lowerHistogram como para el upperHistogram
        Double [] funcionEcualizacionLower = obtenerFuncionDeEcualizacion(lowerHistogram, 0.0, xt);
        
        Double [] funcionEcualizacionUpper = obtenerFuncionDeEcualizacion(upperHistogram, xt + 1, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        int indiceInicialUpper = xt + 1;
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                int intensidadPixelActual = ip.getPixel(i, j);
                if(intensidadPixelActual < indiceInicialUpper){
                    //Si la intensidad actual se encuentra en la parte baja del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionLower[intensidadPixelActual]);
                }else{
                    //Si se ecuentra en la parte superior del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionUpper[intensidadPixelActual - indiceInicialUpper]);
                }
            }
        }
        
        return ip;        
    }
    
    public static ImageProcessor ecualizacionBHEPL(ImageProcessor ip, int [] histogramaTotal, Boolean usarMediana){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        //Array que contiene la propabilidad de ocurrencia de cada intensidad de la imagen
        Double funcionProbabilidad [] = new Double[histogramaTotal.length];
        
        //Numero total de pixeles en la imagen
        double n = altura * ancho;
        
        //Obtenemos la probabilidad de aparicion de cada intensidad 
        for(int i = 0; i < histogramaTotal.length; i++){
            funcionProbabilidad[i] = histogramaTotal[i]/n;
        }
        
        //Obtenemos la intensidad media de los pixeles en la imagen
        Double xm = 0.0;
        for(int i = 0; i < funcionProbabilidad.length; i++){
            xm += funcionProbabilidad[i] * i;
        }
        
        /*Xt viene dado por la formula:
            * Xt = Xm
         *donde:
            * Xm: es la intensidad media de los pixeles en la imagen.
        */
        
        Double xt = xm;
        
        /*En base al Xt hallado procedemos a dividir el histograma total en:
            *lowerHistogram: que posee las intensidades de 0 a Xt.
            *upperHistogram: que posee las intensideades de (Xt + 1) a 255.
        */
        int [] lowerHistogram = new int [xt.intValue() + 1];
        
        //Se halla la longitud del upperHistogram y se inicializa el mismo
        int longitudUpperHistogram = 255 - xt.intValue();
        int [] upperHistogram = new int [longitudUpperHistogram];
        
        //Cargamos cada histograma con sus respectivos valores a partir del histograma total
        int contadorAux = -1;
        for(int i = 0; i < histogramaTotal.length; i++){
            if(i <= xt){
                //Si las intensidades van de 0 - xt, se guardan en lowerHistogram
                lowerHistogram[i] = histogramaTotal[i];
            }else{
                //Si van de (xt+1) a 255 se guarda en upperHistogram
                contadorAux ++;
                upperHistogram[contadorAux] = histogramaTotal[i];
            }
        }
        
        /*Una vez que hemos dividido el histograma total en dos, procedemos a acotar los mismos, 
         * para ello debemos calcular el plateauLimit para cada sub-hsitograma*/
        double lowerPlateauLimit = 0.0;
        double upperPlateauLimit = 0.0;
        
        if(!usarMediana){
            /*Procedemos a hallar el lowerPlateauLimit*/
            int acumuladorAux = 0;
            for(int i = 0; i < lowerHistogram.length; i++){
                acumuladorAux += lowerHistogram[i];
            }

            lowerPlateauLimit = acumuladorAux/(xt + 1);

            /*Procedemos a hallar el upperPlateauLimit*/
            acumuladorAux = 0;
            for(int i = 0; i < upperHistogram.length; i++){
                acumuladorAux += upperHistogram[i];
            }

            upperPlateauLimit = acumuladorAux/(255 - xt);
        }else{
            /*Procedemos a hallar el lowerPlateauLimit*/
            lowerPlateauLimit = obtenerMediana(lowerHistogram);
            
            /*Procedemos a hallar el upperPlateauLimit*/
            upperPlateauLimit = obtenerMediana(upperHistogram);
        }
        
        //Una vez hallado los limites de plateau se procede a hallar los histogramas acotados
        int [] lowerClippedHistogram = acotarHistograma(lowerHistogram, lowerPlateauLimit);
        
        int [] upperClippedHistogram = acotarHistograma(upperHistogram, upperPlateauLimit);
        
        //Usando los histogramas acotados procedemos a hallar sus respectivas funciones de ecualizacion
        Double [] funcionEcualizacionLower = obtenerFuncionDeEcualizacion(lowerClippedHistogram, 0.0, xt.intValue());
        
        Double [] funcionEcualizacionUpper = obtenerFuncionDeEcualizacion(upperClippedHistogram, xt.intValue() + 1, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        int indiceInicialUpper = xt.intValue() + 1;
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                int intensidadPixelActual = ip.getPixel(i, j);
                if(intensidadPixelActual < indiceInicialUpper){
                    //Si la intensidad actual se encuentra en la parte baja del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionLower[intensidadPixelActual]);
                }else{
                    //Si se ecuentra en la parte superior del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionUpper[intensidadPixelActual - indiceInicialUpper]);
                }
            }
        }
        
        return ip;
    }
    
    public static ImageProcessor ecualizacionInfrarojo2PL(ImageProcessor ip, int [] histogramaTotal){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        //Histograma total de la imagen
        //int [] histogramaTotal = ip.getHistogram();
        
        //Numero total de pixeles en la imagen
        double totalPixeles = altura * ancho;
        
        /*Obtenemos la cantidad de posiciones del histograma que son distintas de cero*/
        int l = 0;
        for(int i = 0; i < histogramaTotal.length; i++){
            if(histogramaTotal[i] != 0){
                l++;
            }
        }
        
        /* Definimos la lista 'polar'como la lista de maximos locales encontrados en el histograma 
         * donde 'p' es el tamanho de dicha lista*/
        List<Integer> polar =  new ArrayList<Integer>();
        
        /*Definimos el vector N que representa los elementos del histograma que 
         * poseen valores distintos de cero y cuya longitud es L*/
        int[] n = new int[l];
        int indice = -1;
        for(int i = 0; i < histogramaTotal.length; i++){
            if(histogramaTotal[i] != 0){
                indice ++;
                n[indice] = histogramaTotal[i];
            }
        }
        
        /*Definimos un vector W que representa la ventana usada para buscar los maximos locales, 
         * donde un maximo local se da cuando el elemento central de la ventana es mayor que sus vecinos:
            W_{(n+1)/2} >= max{W1,...,Wn}. Para este caso asumimos n = 5 como en el paper.
         */
        int [] w = new int[5];
        int posicionCentralVentana = (w.length + 1)/2;
        boolean esMaximoLocal;
        
        //Se procede a buscar los maximos locales
        for(int i = 0; i < (n.length - w.length) + 1 ;i++){
            for(int j = 0; j < w.length; j++){
                //Cargamos los valores en la ventana
                w[j] = n[i+j] ;
            }
            //Verificamos si el valor central de la ventana es el maximo en el vecindario
            esMaximoLocal = true;
            for(int j = 0; j < w.length; j++){
                if(j!= posicionCentralVentana && w[j] > w[posicionCentralVentana]){
                    esMaximoLocal = false;
                    break;
                }
            }
            if(esMaximoLocal){
                polar.add(w[posicionCentralVentana]);
            }
        }
        
        /*Hallamos el limite de plateau mas alto en base a los polares hallados siguindo la siguiente formula:
            p(k_{detail}) = Polar_{avg} = (Polar[1]+...+Polar[P])/P
        */
        double limitePlateauUp = 0.0;
        for(Integer valor:polar){
            limitePlateauUp += valor;
        }
        limitePlateauUp = limitePlateauUp/polar.size();
        
        /*El limite de plateau mas bajo puede ser hallado como:
            T_{DOWN} = min{N_{total}, T_{UP} * L}/M
          donde :
            * M es el numero total de grises original(Asumimos que es 256 para una imagen de escala de grises de 8bit).
            * N_{total}: es el numero total de pixeles dentro de la imagen.
            * L: es el total de posiciones dentro del histograma cuyo valor es distinto de cero.
        */
        double limitePlateauDown = (Math.min(totalPixeles, (limitePlateauUp * l)))/256;
        
        //Una vez hallado los limites de plateau se procede a hallar el histograma acotado
        int [] histogramaAcotado = acotarHistogramaMetodoInfrarojo(histogramaTotal, limitePlateauDown, limitePlateauUp);
        
        //Ecualizamos el histograma con los grises reconstruidos a partir del acumulado del histograma acotado
        Double [] funcionEcualizacion = obtenerFuncionDeEcualizacion(histogramaAcotado, 0.0, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                //Se obtiene la intesidad del pixel actual que se usara como indice para la funcion de ecualizacion
                int intensidadPixelActual = ip.getPixel(i, j);
                ip.putPixelValue(i, j, funcionEcualizacion[intensidadPixelActual]);
            }
        }
        
        return ip;
    }
    
    public static ImageProcessor ecualizacionBHE3PL(ImageProcessor ip, int [] histogramaTotal){
        
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        //Array que contiene la propabilidad de ocurrencia de cada intensidad de la imagen
        Double funcionProbabilidad [] = new Double[histogramaTotal.length];
        
        //Numero total de pixeles en la imagen
        double n = altura * ancho;
        
        //Obtenemos la probabilidad de aparicion de cada intensidad 
        for(int i = 0; i < histogramaTotal.length; i++){
            funcionProbabilidad[i] = histogramaTotal[i]/n;
        }
        
        //Obtenemos la intensidad media de los pixeles en la imagen
        Double sp = 0.0;
        for(int i = 0; i < funcionProbabilidad.length; i++){
            sp += funcionProbabilidad[i] * i;
        }
        
        /*Instanciamos los sub-histogramas histogramas de la imagen:
            *lowerHistogram: que posee las intensidades de 0 a SP.
            *upperHistogram: que posee las intensideades de (SP + 1) a 255.
        */
        int [] lowerHistogram = new int [sp.intValue() + 1];
        
        int [] upperHistogram = new int [255 - sp.intValue()];
        
        /*Cargamos cada histograma con sus respectivos valores a partir del histograma total, 
         * ademas hallamos l_min, l_max, pk_l y pk_h donde:
            *l_min: es la intensidad minima que aparece en la imagen.
            *l_max: es la intensidad maxima que aparece en la imagen.
            *pk_l: es el pico de intensidad correspondiente al lowerHistogram.
            *pk_h: es el pico de intensidad correspondiente al upperHistogram*/
        int contadorAux = -1;
        int lMin = 0;
        int lMax = 0;
        int pKl = 0;
        int pKh = 0;
        int tamPicoL  = 0;
        int tamPicoH = 0;
        boolean isLMinFound = false;
        for(int i = 0; i < histogramaTotal.length; i++){
            //Obtenemos los niveles minimos y maximos de grises
            if(!isLMinFound && histogramaTotal[i] != 0){
                lMin = i;
                isLMinFound = true;
            }else if(histogramaTotal[i] != 0){
                lMax = i;
            }
            
            //Se dividen los histogramas basados en las condiciones
            if(i <= sp){
                //Si las intensidades van de 0 - sp, se guardan en lowerHistogram
                lowerHistogram[i] = histogramaTotal[i];
                
                //Se busca el pico maximo de intensidad para el lowerHistogram
                if(lowerHistogram[i] > tamPicoL){
                    tamPicoL = lowerHistogram[i];
                    pKl = i;
                }
            }else{
                //Si van de (sp+1) a 255 se guarda en upperHistogram
                contadorAux ++;
                upperHistogram[contadorAux] = histogramaTotal[i];
                
                //Se busca el pico maximo de intensidad para el upperHistogram
                if(upperHistogram[contadorAux] > tamPicoH){
                    tamPicoH = upperHistogram[contadorAux];
                    pKh = i;
                }
            }
        }
        
        /*Se procede a hallar las intensidades medias en cada sub-histograma:
            *Sp_l: intensidad media del lowerHistogram.
            *Sp_h: intensidad media del upperHistogram.
        */
        int acumuladorAux = 0;
        double cantidadPixeles = 0.0;
        
        //Hallamos la intensidad media para el lowerHistogram
        for(int i = 0; i < lowerHistogram.length; i++){
            acumuladorAux += i * lowerHistogram[i];
            cantidadPixeles += lowerHistogram[i];
        }
        
        double spl = acumuladorAux/cantidadPixeles;
        
        //Hallamos la intensidad media para el upperHistogram
        acumuladorAux = 0;
        cantidadPixeles = 0.0;
        int indiceInicialUpper = sp.intValue() + 1;
        for(int i = 0; i < upperHistogram.length; i++){
            acumuladorAux += (indiceInicialUpper + i) * upperHistogram[i];
            cantidadPixeles += upperHistogram[i];
        }
        
        double sph = acumuladorAux/cantidadPixeles;
        
        /*En base a los valores de l_min, l_max, sp, spl y sph procedemos a hallar las tasas de grises GR comenzando por:
            * GR_L2 = (sp - spl)/(sp - l_min).
            * GR_H2 = (l_max - sph)/(l_max - sp)
        */
        double grL2 = (sp - spl)/(sp - lMin);
        
        double grH2 = (lMax - sph)/(lMax - sp);
        
        /*Una vez hallados los valores de GR_L2 y GR_H2 procedemos a hallar las tasas de diferencias de grises DL y DH:
            *DL = (1 - GR_L2)/2 si GR_L2 > 0.5 o DL = (GR_L2)/2 si GR_L2 <= 0.5.
            *DH = (1 - GR_H2)/2 si GR_H2 > 0.5  o DH = (GR_H2)/2 si GR_H2 <= 0.5.
        */
        double dl = 0.0;
        if(grL2 > 0.5){
            dl = (1 - grL2)/2;
        }else if(grL2 <= 0.5){
            dl = grL2/2;
        }
        
        double dh = 0.0;
        if(grH2 > 0.5){
            dh = (1 - grH2)/2;
        }else if(grH2 <= 0.5){
            dh = grH2/2;
        }
        
        /*Una vez halladas las diferencias de grises DL y DH procedemos a hallar las tasas de grises restantes:
            *GR_L1 = GR_L2 - DL.
            *GR_L3 = GR_L2 + DL.
            *GR_H1 = GR_H2 - DH.
            *GR_H3 = GR_H2 + DH.
        */
        double grL1 = grL2 - dl;
        double grL3 = grL2 + dl;
        double grH1 = grH2 - dh;
        double grH3 = grH2 + dh;
        
        /*Una vez halladas las tasas de grises restantes ya estamos en condiciones 
         *de hallar los limites de plateu siguiendo las siguientes formulas:
            *PL_l1 = GR_L1 * pk_l.
            *PL_l2 = GR_L2 * pk_l.
            *PL_l3 = GR_L3 * pk_l.
            *PL_h1 = GR_H1 * pk_h.
            *PL_h2 = GR_H2 * pk_h.
            *PL_h3 = GR_H3 * pk_h
        */
        
        double limitePlateauL1 = grL1 * pKl;
        double limitePlateauL2 = grL2 * pKl;
        double limitePlateauL3 = grL3 * pKl;
        double limitePlateauH1 = grH1 * pKh;
        double limitePlateauH2 = grH2 * pKh;
        double limitePlateauH3 = grH3 * pKh;
        
        //Una vez hallado los limites de plateau se procede a hallar los histogramas acotados
        int [] lowerClippedHistogram = acotarHistograma(lowerHistogram, limitePlateauL1, limitePlateauL2, limitePlateauL3);
        
        int [] upperClippedHistogram = acotarHistograma(upperHistogram, limitePlateauH1, limitePlateauH2, limitePlateauH3);
        
        //Usando los histogramas acotados procedemos a hallar sus respectivas funciones de ecualizacion
        Double [] funcionEcualizacionLower = obtenerFuncionDeEcualizacion(lowerClippedHistogram, 0.0, sp.intValue());
        
        Double [] funcionEcualizacionUpper = obtenerFuncionDeEcualizacion(upperClippedHistogram, sp.intValue() + 1, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                int intensidadPixelActual = ip.getPixel(i, j);
                if(intensidadPixelActual < indiceInicialUpper){
                    //Si la intensidad actual se encuentra en la parte baja del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionLower[intensidadPixelActual]);
                }else{
                    //Si se ecuentra en la parte superior del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionUpper[intensidadPixelActual - indiceInicialUpper]);
                }
            }
        }
        
        return ip;
    }
    
    public static ImageProcessor ecualizacionBHE2PL(ImageProcessor ip, int [] histogramaTotal){
        int altura = ip.getHeight();
        int ancho = ip.getWidth();
        
        //Array que contiene la propabilidad de ocurrencia de cada intensidad de la imagen
        Double funcionProbabilidad [] = new Double[histogramaTotal.length];
        
        //Numero total de pixeles en la imagen
        double n = altura * ancho;
        
        //Obtenemos la probabilidad de aparicion de cada intensidad 
        for(int i = 0; i < histogramaTotal.length; i++){
            funcionProbabilidad[i] = histogramaTotal[i]/n;
        }
        
        //Obtenemos la intensidad media de los pixeles en la imagen
        Double sp = 0.0;
        for(int i = 0; i < funcionProbabilidad.length; i++){
            sp += funcionProbabilidad[i] * i;
        }
                
        /*Instanciamos los sub-histogramas histogramas de la imagen:
            *lowerHistogram: que posee las intensidades de 0 a SP.
            *upperHistogram: que posee las intensideades de (SP + 1) a 255.
        */
        int [] lowerHistogram = new int [sp.intValue() + 1];
        
        int [] upperHistogram = new int [255 - sp.intValue()];
        
        /*Cargamos cada histograma con sus respectivos valores a partir del histograma total, 
         * ademas hallamos l_min, l_max, pk_l y pk_u donde:
            *l_min: es la intensidad minima que aparece en la imagen.
            *l_max: es la intensidad maxima que aparece en la imagen.
            *pk_l: es el pico de intensidad correspondiente al lowerHistogram.
            *pk_u: es el pico de intensidad correspondiente al upperHistogram*/
        int contadorAux = -1;
        int lMin = 0;
        int lMax = 0;
        int pKl = 0;
        int pKu = 0;
        int tamPicoL  = 0;
        int tamPicoU = 0;
        boolean isLMinFound = false;
        for(int i = 0; i < histogramaTotal.length; i++){
            //Obtenemos los niveles minimos y maximos de grises
            if(!isLMinFound && histogramaTotal[i] != 0){
                lMin = i;
                isLMinFound = true;
            }else if(histogramaTotal[i] != 0){
                lMax = i;
            }
            
            //Se dividen los histogramas basados en las condiciones
            if(i <= sp){
                //Si las intensidades van de 0 - sp, se guardan en lowerHistogram
                lowerHistogram[i] = histogramaTotal[i];
                
                //Se busca el pico maximo de intensidad para el lowerHistogram
                if(lowerHistogram[i] > tamPicoL){
                    tamPicoL = lowerHistogram[i];
                    pKl = i;
                }
            }else{
                //Si van de (sp+1) a 255 se guarda en upperHistogram
                contadorAux ++;
                upperHistogram[contadorAux] = histogramaTotal[i];
                
                //Se busca el pico maximo de intensidad para el upperHistogram
                if(upperHistogram[contadorAux] > tamPicoU){
                    tamPicoU = upperHistogram[contadorAux];
                    pKu = i;
                }
            }
        }
        
        /*Se procede a hallar las intensidades medias en cada sub-histograma:
            *Sp_l: intensidad media del lowerHistogram.
            *Sp_u: intensidad media del upperHistogram.
        */
        int acumuladorAux = 0;
        double cantidadPixeles = 0.0;
        
        //Hallamos la intensidad media para el lowerHistogram
        for(int i = 0; i < lowerHistogram.length; i++){
            acumuladorAux += i * lowerHistogram[i];
            cantidadPixeles += lowerHistogram[i];
        }
        
        double spl = acumuladorAux/cantidadPixeles;
        
        //Hallamos la intensidad media para el upperHistogram
        acumuladorAux = 0;
        cantidadPixeles = 0.0;
        int indiceInicialUpper = sp.intValue() + 1;
        for(int i = 0; i < upperHistogram.length; i++){
            acumuladorAux += (indiceInicialUpper + i) * upperHistogram[i];
            cantidadPixeles += upperHistogram[i];
        }
        
        double spu = acumuladorAux/cantidadPixeles;
        
        /*En base a los valores de l_min, l_max, sp, spl y spu procedemos a hallar las tasas de grises GR comenzando por:
            * GR_L1 = (sp - spl)/(sp - l_min).
            * GR_U1 = (l_max - spu)/(l_max - sp)
        */
        double grL1 = (sp - spl)/(sp - lMin);
        double grU1 = (lMax - spu)/(lMax - sp);
        
        /*Una vez hallados los valores de GR_L1 y GR_U1 procedemos a hallar las tasas de diferencias de grises DL y DU:
            *DL = (1 - GR_L1)/2 si GR_L1 > 0.5 o DL = (GR_L1)/2 si GR_L1 <= 0.5.
            *DU = (1 - GR_H1)/2 si GR_H1 > 0.5  o DU = (GR_H1)/2 si GR_H1 <= 0.5.
        */
        double dl = 0.0;
        if(grL1 > 0.5){
            dl = (1 - grL1)/2;
        }else if(grL1 <= 0.5){
            dl = grL1/2;
        }
        
        double du = 0.0;
        if(grU1 > 0.5){
            du = (1 - grU1)/2;
        }else if(grU1 <= 0.5){
            du = grU1/2;
        }
        
        /*Una vez halladas las diferencias de grises DL y DU procedemos a hallar las tasas de grises restantes:
            *GR_L2 = GR_L1 + DL.
            *GR_U2 = GR_U1 + DU.
        */
        double grL2 = grL1 + dl;
        double grU2 = grU1 + du;
        
        /*Una vez halladas las tasas de grises restantes ya estamos en condiciones 
         *de hallar los limites de plateu siguiendo las siguientes formulas:
            *PL_l1 = GR_L1 * pk_l.
            *PL_l2 = GR_L2 * pk_l.
            *PL_u1 = GR_U1 * pk_u.
            *PL_u2 = GR_U2 * pk_u.
        */
        
        double limitePlateauL1 = grL1 * pKl;
        double limitePlateauL2 = grL2 * pKl;
        double limitePlateauU1 = grU1 * pKu;
        double limitePlateauU2 = grU2 * pKu;
        
        //Una vez hallado los limites de plateau se procede a hallar los histogramas acotados
        int [] lowerClippedHistogram = acotarHistograma(lowerHistogram, limitePlateauL1, limitePlateauL2);
        
        int [] upperClippedHistogram = acotarHistograma(upperHistogram, limitePlateauU1, limitePlateauU2);
        
        //Usando los histogramas acotados procedemos a hallar sus respectivas funciones de ecualizacion
        Double [] funcionEcualizacionLower = obtenerFuncionDeEcualizacion(lowerClippedHistogram, 0.0, sp.intValue());
        
        Double [] funcionEcualizacionUpper = obtenerFuncionDeEcualizacion(upperClippedHistogram, sp.intValue() + 1, 255.0);
        
        /*Se recorre la imagen y se reemplaza el valor de intensidad dada la siguiente relacion:
            Y = Y(i,j) = f_l(X_l) union f_u(X_u) donde:
            * f_l(X_l) = f_l(X(i,j)| para todo X(i,j) perteneciente a X_l.
            * f_u(X_u) = f_u(X(i,j)| para todo X(i,j) perteneciente a X_u.
        */
        for(int j = 0; j < altura; j++){
            for(int i = 0; i < ancho; i++){
                int intensidadPixelActual = ip.getPixel(i, j);
                if(intensidadPixelActual < indiceInicialUpper){
                    //Si la intensidad actual se encuentra en la parte baja del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionLower[intensidadPixelActual]);
                }else{
                    //Si se ecuentra en la parte superior del histograma
                    ip.putPixelValue(i, j, funcionEcualizacionUpper[intensidadPixelActual - indiceInicialUpper]);
                }
            }
        }
        
        return ip;
    }
    
    private static Double[] obtenerFuncionDeEcualizacion(int[] histograma, double x0, double xl){
        
        /*Se inicializan los arrays:
            *  funcionEcualizacion.
            *  funcionProbabilidad.
            *  histogramaAcumulado.
         Con la longitud del histograma recibido como parametro*/
        
        //Histograma que sera devuelto por la funcion al finalizar la ecualizacion
        Double funcionEcualizacion [] = new Double[histograma.length] ;
        
        //Array que contiene la propabilidad de ocurrencia de cada intensidad de la imagen
        Double funcionProbabilidad [] = new Double[histograma.length] ;
        
        //Array que representa el histograma acumulado normalizado del histograma que se recibe como parametro
        Double histogramaAcumulado [] = new Double[histograma.length] ;
        
        //N representa el numero de pixeles que contiene la imagen
        double n = 0;
        
        //Obtenemos el numero total de pixeles de la imagen a partir del histograma
        for(int i = 0; i < histograma.length; i++){
            n += histograma[i];
        }
        
        //Obtenemos la probabilidad de aparicion de cada intensidad 
        for(int i = 0; i < histograma.length; i++){
            funcionProbabilidad[i] = histograma[i]/n;
        }
        
        /*Obtenemos el histograma acumulado normalizado mediante la sumarizacion de las probabilidades
         *obtenidas anteriormente*/
        Double acumuladorAux = 0.0;
        for(int i = 0; i < funcionProbabilidad.length; i++){
            acumuladorAux += funcionProbabilidad[i];
            histogramaAcumulado[i] = acumuladorAux;
        }
        
        /*Se procede a hallar la funcion de ecualizacion, mediante la formula:
            f(x)=X_0 + (X_L-1 - X_0) *[c(x) - 0.5*p(x)]
          Donde:
            * X_0 es el menor nivel de intensidad.
            * X_L-1 es el mayor nivel de intensidad, en el codigo sera presendato como 'xl'.
            * c(x) es el histograma acumulado normalizado de la imagen.
            * p(x) es la funcion de probabilidades perteneciente a la imagen.
        */
        for(int i = 0; i < histogramaAcumulado.length; i++){
            //Se redondea la operacion usando el redondeo tradicional
            //Double valorRedondeado = Math.rint(0.0 + ((255.0 - 0.0) * (histogramaAcumulado[i] - (0.5 * funcionProbabilidad[i]))));
            //Se almacena el valor redondeado en la funcion de ecualizacion
            funcionEcualizacion[i] = x0 + ((xl - x0) * (histogramaAcumulado[i] - (0.5 * funcionProbabilidad[i])));
        }
        return funcionEcualizacion;
    }
    
    private static int[] acotarHistograma (int [] histograma, double plateauLimit){
        
        /*Histograma que poseera los valores acotados del histograma que se pasa por parametro y 
         *cuyo limite es el plateauLimit que tambien se pasa por parametro*/
        int[] histogramaAcotado = new int[histograma.length];
        
        //Cargamos el histograma acotado con los valores habiles del histograma original
        for(int i = 0; i < histograma.length; i++){
            if(histograma[i] <= plateauLimit){
                /*Si el valor es menor o igual al limite impuesto se carga dicho valor en 
                 *el histograma acotado*/
                histogramaAcotado[i] = histograma[i];
            }else{
                //Si supera el limite, se setea el limite de plateau en el histograma acotado
                histogramaAcotado[i] = (Double.valueOf(plateauLimit)).intValue();
            }
        }
        
        return histogramaAcotado;
    }
    
    private static int[] acotarHistograma (int [] histograma, double pl1, double pl2){
        /*Histograma que poseera los valores acotados del histograma que se pasa por parametro y 
         *cuyo limites son los plateus limits pl1 y pl2 pasados como parametros.*/
        int[] histogramaAcotado = new int[histograma.length];
        
        //Cargamos el histograma acotado con los valores habiles del histograma original
        for(int i = 0; i < histograma.length; i++){
            if(histograma[i] <= pl2){
                /*Si el valor es menor o igual pl2 se carga el valor de pl1 en 
                 *el histograma acotado*/
                histogramaAcotado[i] = Double.valueOf(pl1).intValue();
            }else if(histograma[i] > pl2){
                //Si supera a pl2 se carga el valor de pl2 en el histograma acotado
                histogramaAcotado[i] = Double.valueOf(pl2).intValue();
            }
        }
        
        return histogramaAcotado;
    }
    
    private static int[] acotarHistogramaMetodoInfrarojo(int [] histograma, double pl1, double pl2){
        /*Histograma que poseera los valores acotados del histograma que se pasa por parametro y 
         *cuyo limites son los plateus limits pl1 y pl2 pasados como parametros.*/
        int[] histogramaAcotado = new int[histograma.length];
        
        //Cargamos el histograma acotado con los valores habiles del histograma original
        for(int i = 0; i < histograma.length; i++){
            if(histograma[i] >= pl2){
                //Si el valor es mayor o igual a limite superior
                histogramaAcotado[i] = Double.valueOf(pl2).intValue();
            }else if(histograma[i] >= pl1 && histograma[i] < pl2){
                //Si el valor es mayor o igual al limite inferior y menor al limite superior
                histogramaAcotado[i] = histograma[i];
            }else if(histograma[i] > 0 && histograma[i] < pl1){
                //Si el valor es mayor a cero y menor al limite inferior
                histogramaAcotado[i] = Double.valueOf(pl1).intValue();
            }else if(histograma[i] == 0){
                //Si el valor es igual a cero
                histogramaAcotado[i] = 0;
            }
        }
        
        return histogramaAcotado;
    }
    
    private static int[] acotarHistograma (int [] histograma, double pl1, double pl2, double pl3){
        /*Histograma que poseera los valores acotados del histograma que se pasa por parametro y 
         *cuyo limites son los plateus limits pl1, pl2 y pl3 pasados como parametros.*/
        int[] histogramaAcotado = new int[histograma.length];
        
        //Cargamos el histograma acotado con los valores habiles del histograma original
        for(int i = 0; i < histograma.length; i++){
            if(histograma[i] <= pl1){
                /*Si el valor es menor o igual pl1 se carga dicho valor en 
                 *el histograma acotado*/
                histogramaAcotado[i] = Double.valueOf(pl1).intValue();
            }else if( pl1 < histograma[i] && histograma[i] <= pl3){
                //Si se encuentra entre pl1 y pl3 se setea pl2 en el histograma acotado
                histogramaAcotado[i] = Double.valueOf(pl2).intValue();
            }else if(histograma[i] > pl3){
                //Si supera a pl3 se carga el valor de pl3 en el histograma acotado
                histogramaAcotado[i] = Double.valueOf(pl3).intValue();
            }
        }
        
        return histogramaAcotado;
    }
    
    private static double obtenerMediana(int[] histograma){
        
        //Valor de la mediana
        double mediana = 0.0;
        
        //Vector auxiliar que nos ayudara a hallar la mediana
        int[] vectorAux = new int[histograma.length];
        
        //Copiamos en vectorAux el histograma que se nos pasa como parametro
        System.arraycopy(histograma, 0, vectorAux, 0, vectorAux.length);
        
        //Ordenamos el vector auxiliar
        Quicksort(vectorAux, 0, vectorAux.length - 1);
        
        if(vectorAux.length % 2 == 0){
            //En caso de que el tamanho del array sea par
            int medio = vectorAux.length /2;
            mediana = (vectorAux[medio] + vectorAux[medio-1])/2.0;
        }else{
            //En caso de que el tamanho del array sea impar
            int medio = vectorAux.length /2;
            mediana = vectorAux[medio + 1];
        }
        
        return mediana;
    }
    
    /*Metodo de ordenacion quicksort*/
    private static void Quicksort(int[] vector, int first, int last){
        
        int i=first, j=last;
        int pivote=vector[(first + last) / 2];
        int auxiliar;
        do{
            while(vector[i] < pivote) i++;      
            while(vector[j] > pivote) j--;
            if (i <= j){
                auxiliar=vector[j];
                vector[j]=vector[i];
                vector[i]=auxiliar;
                i++;
                j--;
            }
        }while (i <= j);
 
        if(first < j){
            Quicksort(vector, first, j);
        }

        if(last > i){
            Quicksort(vector, i, last);
        }
    }
}

