package utils;

/**
 *
 * @author fdaumas
 */


import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Imagen {
	public BufferedImage srcImage;
	public File imgFile;
	int width;
	int height;
	int type;
	Raster raster;
	DataBuffer buffer;
	byte[] srcData;
	byte[] dstData;
	private int[][] matriz;
	private int [] vectorMatriz;
	private int [] histograma;
	private Integer minNivelGris;
	private Integer maxNivelGris;
	private Integer cantPixeles;
	private Integer maxLevelValue;
	private List<BigDecimal> densidadO;
	private int [] LUT;
	
	private Double media;
	private Double contraste;
	private int[][] matrizThreshold;

	



	public int[] getHistograma() {
		if (srcData== null){ histograma= new int[256];}
		if (histograma==null){
			int ptr;
			histograma = new int[256];
			// Clear histogram data
			// Set all values to zero
			ptr = 0;
			maxLevelValue = 0;
			while (ptr < srcData.length) {
				int h = 0xFF & srcData[ptr];
				histograma[h]++;
				if (histograma[h] > maxLevelValue)
					maxLevelValue = histograma[h];
				ptr++;
			}
		}
		
		
		return histograma;
	}

	

	public void setHistograma(int[] histograma) {
		this.histograma = histograma;
	}



	public BufferedImage getSrcImage() {
		return srcImage;
	}

	public void setSrcImage(BufferedImage srcImage) {
		this.srcImage = srcImage;
	}

	public File getImgFile() {
		return imgFile;
	}

	public void setImgFile(File imgFile) {
		this.imgFile = imgFile;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Raster getRaster() {
		return raster;
	}

	public void setRaster(Raster raster) {
		this.raster = raster;
	}

	public DataBuffer getBuffer() {
		return buffer;
	}

	public void setBuffer(DataBuffer buffer) {
		this.buffer = buffer;
	}

	public byte[] getSrcData() {
		return srcData;
	}

	public void setSrcData(byte[] srcData) {
		this.srcData = srcData;
	}

	public byte[] getDstData() {
		return dstData;
	}

	public void setDstData(byte[] dstData) {
		this.dstData = dstData;
	}
	
	public void contruirImagen(){
		
		this.width = srcImage.getWidth();
		this.height = srcImage.getHeight();
		raster = srcImage.getData();
		buffer = raster.getDataBuffer();
		type = buffer.getDataType();

		if (type != DataBuffer.TYPE_BYTE) {
			System.err.println("Wrong image data type");
			System.exit(1);
		}
		if (buffer.getNumBanks() != 1) {
			System.err.println("Wrong image data format");
			System.exit(1);
		}

		DataBufferByte byteBuffer = (DataBufferByte) buffer;
		srcData = byteBuffer.getData(0);

		// Sanity check image
		if (width * height != srcData.length) {
			System.err
					.println("Unexpected image data size. Should be greyscale image");
			System.exit(1);
		}

		dstData = new byte[srcData.length];
		getHistograma();
	}
	
	public Imagen(BufferedImage image){
		this.srcImage = image;
		contruirImagen();
	}

	public Imagen(String filename) {

		// Load Source image
		srcImage = null;

		try {
			imgFile = new File(filename);
			srcImage = javax.imageio.ImageIO.read(imgFile);
		} catch (IOException ioE) {
			System.err.println(ioE);
			System.exit(1);
		}

		contruirImagen();
		
	}
	
	 public double getMedia() {
		if (media == null) {
			double sum = 0.0;

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					sum += raster.getSample(x, y, 0);
				}
			}
			media = sum / (height * width);
		}
		return media;
	}
	 
	 
	
	
	public int getMinNivelGris(){
		if(minNivelGris==null){
			int band = 0;
			cantPixeles=0;
			for (int i = 0; i <256; i++) {

				cantPixeles += histograma[i];
				
				if (band == 0 && histograma[i] != 0) {
					minNivelGris = i;
					band = 1;
				}
				if (histograma[i] != 0) {
					maxNivelGris = i;
				}
			}
		}
		return minNivelGris;
	}
	
	public int getMaxNivelGris(){
		if (maxNivelGris==null){
			getMinNivelGris();
		}
		return maxNivelGris;
		
	}
	
	public int getCantPixeles(){
		if(cantPixeles==null){
			getMinNivelGris();
		}
		return cantPixeles;
	}
	
	public Integer getMaxLevelValue() {
		if(maxLevelValue==null){
			getHistograma();
		}
		return maxLevelValue;
	}
	
	public int [] [] getMatrizOriginal() {
		if (this.matriz==null){
			matriz = new int[this.srcImage.getWidth()][this.srcImage.getHeight()];
			vectorMatriz = new int [this.getCantPixeles()];
			// BufferedImage srcImage = imagen.getSrcImage();
			int l = 0;
			for (int i = 0; i < this.srcImage.getWidth(); i++) {
				for (int j = 0; j < this.srcImage.getHeight(); j++) {
					int h = 0xFF & this.srcImage.getRGB(i, j);
					matriz[i][j] = h;
					vectorMatriz[l] = h;
					l++;
				}
			}
		}
		return matriz;
	}
	
	public int [] getMatrizArray(){
		if (this.matriz==null)
			getMatrizOriginal();
		return this.vectorMatriz;
	}
	
	
	
	
	public Double getFDA(int k){
		Double fda = 0D;
		for (int i =0; i<k; i++){
			fda += getDensidad().get(k).doubleValue() ;
		}
		return fda;
	}
	
	public List<BigDecimal> getDensidad() {
		if(densidadO==null){
			densidadO = new ArrayList<BigDecimal>();
			for (int i = 0; i < 256; i++) {
				densidadO.add(i, (new BigDecimal(this.getHistograma()[i])).divide(new BigDecimal(this.getCantPixeles()), 20, RoundingMode.HALF_EVEN));
			}
		}
		return densidadO;
	}



	public int[] getLUT() {
		return LUT;
	}



	public void setLUT(int[] lUT) {
		LUT = lUT;
	}


	public Double getContraste() {
		if (this.contraste==null){
			this.contraste = ProblemUtils.calcularContraste2(this);
		}
		return this.contraste;
	}
	
	
	
	public int[][] getMatrizThreshold(int umbral) {

		matrizThreshold = new int[this.getWidth()][this.getHeight()];
		BufferedImage srcImage = this.getSrcImage();

		for (int i = 0; i < this.getWidth(); i++) {
			for (int j = 0; j < this.getHeight(); j++) {
				int h = 0xFF & srcImage.getRGB(i, j);
				if (h <= umbral) {
					matrizThreshold[i][j] = 0;
				} else if (h > umbral) {
					matrizThreshold[i][j] = 1;
				}
			}
		}
		return matrizThreshold;

	}






}