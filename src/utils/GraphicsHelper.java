package utils;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import otsus.GreyFrame;
import otsus.OtsuThresholder;

public class GraphicsHelper {
	
	public static void showHistogramFrame(OtsuThresholder thresholder, int[] histogramaOriginal, int[] histogramaback, int[] histogramafore ){
		GreyFrame histBack = createHistogramFrame(histogramaback, thresholder);
		GreyFrame histFore = createHistogramFrame(histogramafore, thresholder);
		GreyFrame histogramGreyFore = createHistogramFrame(histogramaOriginal, thresholder);
		
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		panel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
		panel.add(histBack, BorderLayout.WEST);
		panel.add(histFore, BorderLayout.EAST);
		panel.add(histogramGreyFore, BorderLayout.NORTH);
		panel.add(new JLabel("Mohe APP",
				JLabel.CENTER), BorderLayout.SOUTH);
	
		JFrame frame = new JFrame("Mohe");
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	
	
	public static void showHistogramFrame(Imagen thresholder, int[] histogramaOriginal, int[] histogramaback, int[] histogramafore ){
		GreyFrame histBack = createHistogramFrame(histogramaback, thresholder);
		GreyFrame histFore = createHistogramFrame(histogramafore, thresholder);
		GreyFrame histogramGreyFore = createHistogramFrame(histogramaOriginal, thresholder);
		
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		panel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
		panel.add(histBack, BorderLayout.WEST);
		panel.add(histFore, BorderLayout.EAST);
		panel.add(histogramGreyFore, BorderLayout.NORTH);
		panel.add(new JLabel("Mohe APP",
				JLabel.CENTER), BorderLayout.SOUTH);
	
		JFrame frame = new JFrame("Mohe");
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	
	
	public static void showHistogramFrame(JComponent north, JComponent west, JComponent east,  JComponent center ){
		
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		panel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
		if (west!=null)panel.add(west, BorderLayout.WEST);
		if (east!=null)panel.add(east, BorderLayout.EAST);
		if (north!=null)panel.add(north, BorderLayout.NORTH);
		if (center!=null)panel.add(center, BorderLayout.CENTER);
		panel.add(new JLabel("Mohe APP",
				JLabel.CENTER), BorderLayout.SOUTH);

		JFrame frame = new JFrame("Mohe");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	
	
	public static GreyFrame createHistogramFrame(int[] histData, OtsuThresholder thresholder) {
		int numPixels = 256 * 100;
		byte[] histPlotData = new byte[numPixels];

		// int[] histData = thresholder.getHistData();
		thresholder.histData= histData;
		int max = thresholder.getMaxLevelValue();

		for (int p = 0; p < 256; p++) {
			int ptr = (numPixels - 256) + p;
			int val = (100 * histData[p]) / max;
			for (int i = 0; i < 100; i++, ptr -= 256)
				histPlotData[ptr] = (val < i) ? (byte) 255 : 0;

		}

		return new GreyFrame(256, 100, histPlotData);
	}
	
	public static GreyFrame createHistogramFrame(OtsuThresholder thresholder) {
		int numPixels = 256 * 100;
		byte[] histPlotData = new byte[numPixels];

		int[] histData = thresholder.getHistData();
		int max = thresholder.getMaxLevelValue();
		int threshold = thresholder.getThreshold();

		for (int l = 0; l < 256; l++) {
			int ptr = (numPixels - 256) + l;
			int val = (100 * histData[l]) / max;

			if (l == threshold) {
				for (int i = 0; i < 100; i++, ptr -= 256)
					histPlotData[ptr] = (byte) 128;
			} else {
				for (int i = 0; i < 100; i++, ptr -= 256)
					histPlotData[ptr] = (val < i) ? (byte) 255 : 0;
			}
		}

		return new GreyFrame(256, 100, histPlotData);
	}
	
	
	public static GreyFrame createHistogramFrame(Imagen imagen) {
		int numPixels = 256 * 100;
		byte[] histPlotData = new byte[numPixels];

		int[] histData = imagen.getHistograma();
		int max = imagen.getMaxLevelValue();

		for (int l = 0; l < 256; l++) {
			int ptr = (numPixels - 256) + l;
			int val = (100 * histData[l]) / (max); //35
			for (int i = 0; i < 100; i++, ptr -= 256)
				histPlotData[ptr] = (val < i) ? (byte) 255 : 0;
		}

		return new GreyFrame(256, 100, histPlotData);
	}
	
	
	public static GreyFrame createHistogramFrame(int[] histData, Imagen imagen) {
		int numPixels = 256 * 100;
		byte[] histPlotData = new byte[numPixels];

		int max = imagen.getMaxLevelValue();

		for (int l = 0; l < 256; l++) {
			int ptr = (numPixels - 256) + l;
			int val = (100 * histData[l]) / max;
			for (int i = 0; i < 100; i++, ptr -= 256)
				histPlotData[ptr] = (val < i) ? (byte) 255 : 0;
		}

		return new GreyFrame(256, 100, histPlotData);
	}
	
	
	public static void imprimirFrame(String ruta, String nombreImagen, Imagen imagenOriginal, Imagen imagenNueva, String algoritmo) {

		String formato = "png";

		File fichero = new File(ruta + nombreImagen + "_he." + formato);
		try {
			ImageIO.write(imagenNueva.getSrcImage(), formato, fichero);
		} catch (IOException e) {
			System.out.println("Error de escritura");
		}

		// Create GUI
		GreyFrame srcFrame = new GreyFrame(imagenOriginal.getWidth(),
				imagenOriginal.getHeight(), imagenOriginal.getSrcData());
		GreyFrame improvedFrame = new GreyFrame(imagenNueva.getWidth(),
				imagenNueva.getHeight(), imagenNueva.getSrcData());
		GreyFrame histFrame = GraphicsHelper.createHistogramFrame(imagenOriginal);
		GreyFrame histFrame2 = GraphicsHelper.createHistogramFrame(imagenNueva);
		
		

		JPanel infoPanel = new JPanel();
		infoPanel.add(histFrame);
		JPanel infoPanel2 = new JPanel();
		infoPanel2.add(histFrame2);

		GraphicsHelper.showHistogramFrame(infoPanel, srcFrame, improvedFrame, infoPanel2);
		
		// Save Images
		try {
			String basename = ruta + nombreImagen;

			javax.imageio.ImageIO.write(histFrame2.getBufferImage(), "PNG",
					new File(basename + "_hist_" + algoritmo + ".png"));
			javax.imageio.ImageIO.write(improvedFrame.getBufferImage(), "PNG",
					new File(basename + "_enh_" + algoritmo + ".png"));

		} catch (IOException ioE) {
			System.err.println("Could not write image " + ruta);
		}

	}
	
	public static void saveToFolder(String ruta, String nombreImagen, String formato, Imagen imagenOriginal, Imagen imagenNueva, String algoritmo) {
		GreyFrame histFrame2 = GraphicsHelper.createHistogramFrame(imagenNueva);
		File ficheroImg = new File(ruta + nombreImagen + "_" + algoritmo + "_e." + "png");
		
		File ficheroHist = new File(ruta + nombreImagen + "_" + algoritmo + "_e_hist." + formato);
		try {
			ImageIO.write(imagenNueva.getSrcImage(), "png", ficheroImg);
			ImageIO.write(histFrame2.getBufferImage(), formato, ficheroHist);
			
		} catch (IOException e) {
			System.out.println("Error de escritura");
		}
		
	}

}
