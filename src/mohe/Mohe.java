package mohe;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import jmetal.core.Variable;
import jmetal.util.JMException;
import otsus.GreyFrame;
import otsus.OtsuThresholder;
import utils.GraphicsHelper;
import utils.Imagen;

public class Mohe {

	private int[][] matrizThreshold;
	private int[] histograma;
	private OtsuThresholder thresholder;
	private int threshold;

	public int getThreshold() {
		return threshold;
	}

	Logger logger = Logger.getLogger("MoheNew");

	public int[][] calcularMatrizThreshold(Imagen imagen, int umbral) {

		int[][] matrizThreshold = new int[imagen.getWidth()][imagen.getHeight()];
		BufferedImage srcImage = imagen.getSrcImage();

		for (int i = 0; i < imagen.getWidth(); i++) {
			for (int j = 0; j < imagen.getHeight(); j++) {
				int h = 0xFF & srcImage.getRGB(i, j);
				if (h <= umbral) {
					matrizThreshold[i][j] = 0;
				} else if (h > umbral) {
					matrizThreshold[i][j] = 1;
				}
			}
		}
		return matrizThreshold;

	}
	
	public Imagen mohe(Imagen imagenOtsus, BigDecimal a,
			BigDecimal b, BigDecimal c, BigDecimal d, boolean showGraphicDetails) {
		return this.mohe(imagenOtsus,a,b,c,d,null,showGraphicDetails);
	}
	
	public Imagen mohe(Imagen imagenOtsus, BigDecimal a,
			BigDecimal b, BigDecimal c, BigDecimal d) {
		return this.mohe(imagenOtsus,a,b,c,d,null, false);
	}

	public Imagen mohe(Imagen imagenOtsus, BigDecimal a,
			BigDecimal b, BigDecimal c, BigDecimal d, Integer threshold, boolean showGraphicDetails) {

		int width = imagenOtsus.getWidth();
		int height = imagenOtsus.getHeight();

		logger.info("valores a,b,c,d: " + a + ", " +b + ", "+c+ ", "+d);
		thresholder = new OtsuThresholder();
		if (threshold==null) {// use OtsusMethod
			threshold = thresholder.doThreshold(imagenOtsus.getSrcData(),
				imagenOtsus.getDstData());
		}


		histograma = histograma!=null?histograma:imagenOtsus.getHistograma();
		
		/*
		 * Obtiene la cantidad de pixeles del back asi como el max y min nivel
		 * de gris
		 */
		
		BigDecimal maxNivelGris = BigDecimal.ZERO;
		BigDecimal minNivelGris = new BigDecimal(256);
		int band = 0;
		for (int i = 0; i < 256; i++) {
			
			/*			if (new BigDecimal(histograma[i]).compareTo(minNivelGris) < 0 ) {
				minNivelGris = new BigDecimal(i);
				band = 1;
			}*/
			
			if (histograma[i] != 0 && band == 0) {
				minNivelGris = new BigDecimal(i);
				
				if (threshold<=minNivelGris.intValue()){
					threshold = minNivelGris.intValue()+1;
				}
				
				band = 1;
			}
			if (histograma[i] != 0) {
				maxNivelGris = new BigDecimal(i);
			}
		}
		
		if (threshold>=maxNivelGris.intValue()){
			threshold = maxNivelGris.intValue()-1;
		}
		
		logger.info("Threshold: " + threshold);
		this.threshold=threshold;


		matrizThreshold = matrizThreshold!=null?matrizThreshold:calcularMatrizThreshold(imagenOtsus, threshold);
		;

		
		//System.out.printf("El maximo nivel de gris de la imagen es %d \n",maxNivelGris.intValue());
		//System.out.printf("El minimo nivel de gris de la imagen es %d \n",minNivelGris.intValue());
		band =0;
		int maxNivelGrisBack = 0;
		int minNivelGrisBack = 0;
		double cantPixelBack = 0.0;

		for (int i = 0; i <= threshold; i++) {

			cantPixelBack += histograma[i];
			
			if (histograma[i] != 0 && band == 0) {
				minNivelGrisBack = i;
				band = 1;
			}
			if (histograma[i] != 0) {
				maxNivelGrisBack = i;
			}
		}
		double cantPixelFore = 0.0;
		int maxNivelGrisFore = 0;
		int minNivelGrisFore = 0;
		int cantElementosFore = 0;
		band = 0;

		for (int i = (threshold + 1); i < 256; i++) {
			cantPixelFore += histograma[i];
			cantElementosFore++;
			if (histograma[i] != 0 && band == 0) {
				minNivelGrisFore = i;
				band = 1;
			}
			if (histograma[i] != 0) {
				maxNivelGrisFore = i;
			}
		}
		//
		/* logger.info("Total de Pixeles Fore: " + cantPixelFore);
		 logger.info("Maximo NIvel de GRis Fore: "+ maxNivelGrisFore);
		 logger.info("Minimo Nivel de GRis FOre: "+ minNivelGrisFore);*/
		//
		/* calcular la probabilidad de densidad back */
		List<BigDecimal> densidad = new ArrayList<BigDecimal>();
		BigDecimal maxDensidadBack = BigDecimal.ZERO;
		BigDecimal sumTotalDensidadBack = BigDecimal.ZERO;
		BigDecimal promedioDensidadBack = BigDecimal.ZERO;

		int [] histogramaback = new int[256];
		int l = 0;
		for (int i = 0; i <= threshold; i++, l++) {
			densidad.add(i, (new BigDecimal(histograma[i])).divide(
					new BigDecimal(cantPixelBack), 20, RoundingMode.HALF_EVEN));
			sumTotalDensidadBack = sumTotalDensidadBack.add(densidad.get(i));
			if (densidad.get(i).compareTo(maxDensidadBack) > 0) {
				maxDensidadBack = densidad.get(i);
			}
			//logger.info("Densidad en : " + i+ " es "+densidad.get(i));
			histogramaback[l] = ((Double)(densidad.get(i).doubleValue() * 10000)).intValue(); 
			
			
		}
			
		promedioDensidadBack = sumTotalDensidadBack.divide(new BigDecimal(
				threshold+1), 20, RoundingMode.HALF_EVEN);

		
		BigDecimal maxDensidadFore = BigDecimal.ZERO;
		BigDecimal sumTotalDensidadFore = BigDecimal.ZERO;
		BigDecimal promedioDensidadFore = BigDecimal.ZERO;
		/* calcular la probabilidad de densidad fore */
		int [] histogramafore = new int[256];
		l = 0;
		for (int i = threshold+1; i < 256; i++, l++) {
			
			densidad.add(i, (new BigDecimal(histograma[i])).divide(
					new BigDecimal(cantPixelFore), 20, RoundingMode.HALF_EVEN));
			sumTotalDensidadFore = sumTotalDensidadFore.add(densidad.get(i));
			if (densidad.get(i).compareTo(maxDensidadFore) > 0) {
				maxDensidadFore = densidad.get(i);
			}
			histogramafore[l] = ((Double)(densidad.get(i).doubleValue() * 10000)).intValue();
			
		}
		
		if (showGraphicDetails){
			GraphicsHelper.showHistogramFrame(thresholder, histograma, histogramaback, histogramafore);
		}
		
		promedioDensidadFore = sumTotalDensidadFore.divide(new BigDecimal(
				cantElementosFore), 20, RoundingMode.HALF_EVEN);

		/*
		 * BigDecimal a = new BigDecimal(0.9); BigDecimal b = new
		 * BigDecimal(0.6); BigDecimal c = new BigDecimal(0.9); BigDecimal d =
		 * new BigDecimal(0.6);
		 */

		/* Aplicar las restricciones "a y b" al back */
		/* calcular los valores de alfa y beta */
		BigDecimal alfa = maxDensidadBack.multiply(b);
		BigDecimal beta = new BigDecimal(0.0001);
		
		if (alfa.compareTo(beta) < 0){
			System.out.println("Alfa es menor a Beta");
			
		}

		BigDecimal sumTotalTransDensidadBack = BigDecimal.ZERO;
		BigDecimal promedioTransDensidadBack = BigDecimal.ZERO;

		List<BigDecimal> transDensidad = new ArrayList<BigDecimal>();
		int [] histogramabackd = new int[256];
		int [] histogramafored = new int[256]; 

		
		
		
		for (int i = 0; i <= threshold; i++) {

			if (densidad.get(i).compareTo(alfa) > 0) {
				transDensidad.add(i, alfa);

			} else if (densidad.get(i).compareTo(alfa) <= 0
					&& densidad.get(i).compareTo(beta) >= 0) {

				BigDecimal potencia = new BigDecimal(
						Math.pow(
								((densidad.get(i).subtract(beta)).divide(
										alfa.subtract(beta), 20,
										RoundingMode.HALF_EVEN)).doubleValue(),
								a.doubleValue()));
				transDensidad.add(i, potencia.multiply(alfa));
			} else if (densidad.get(i).compareTo(beta) < 0) {
				transDensidad.add(i, BigDecimal.ZERO);
			} else {
				System.out.println("ALGO ANDA MAL");
			}
			sumTotalTransDensidadBack = sumTotalTransDensidadBack
					.add(transDensidad.get(i));
			histogramabackd[i] = new Double(transDensidad.get(i).doubleValue() *10000).intValue();
			
		}
		promedioTransDensidadBack = sumTotalTransDensidadBack.divide(
				new BigDecimal(threshold+1), 20, RoundingMode.HALF_EVEN);

		BigDecimal promedioErrorTotalBack = promedioTransDensidadBack
				.subtract(promedioDensidadBack);

		BigDecimal sum = BigDecimal.ZERO;
		for (int i = 0; i <= threshold; i++) {
			sum = transDensidad.get(i).add(promedioErrorTotalBack);
			transDensidad.add(i, sum);
			sum = BigDecimal.ZERO;
		}

		/* Aplicar las restricciones "c y d" al fore */
		/* calcular los valores de delta y tita */
		BigDecimal delta = d.multiply(maxDensidadFore);
		BigDecimal tita = promedioDensidadFore;
		/*System.out.printf("El valor de delta es %f \n",delta.doubleValue());
		System.out.printf("El valor de tita es %f \n",tita.doubleValue());*/
		
		BigDecimal sumTotalTransDensidadFore = BigDecimal.ZERO;
		BigDecimal promedioTransDensidadFore = BigDecimal.ZERO;
		
		for (int i = threshold +1; i < 256; i++) {

			if (densidad.get(i).compareTo(delta) > 0) {
				transDensidad.add(i, delta);
			} else if (densidad.get(i).compareTo(delta) <= 0
					&& densidad.get(i).compareTo(tita) >= 0) {
				// ((densidad.get(i) - tita) / (delta - tita)), c);
				BigDecimal potencia2 = new BigDecimal(Math.pow(((densidad
						.get(i).subtract(tita)).divide(delta.subtract(tita),
						20, RoundingMode.HALF_EVEN)).doubleValue(), c
						.doubleValue()));
				transDensidad.add(i, potencia2.multiply(delta));
			} else if (densidad.get(i).compareTo(tita) < 0) {
				transDensidad.add(i, tita);
			} else {
				System.out.println("ALGO ANDA MAL 02");
			}
			sumTotalTransDensidadFore = sumTotalTransDensidadFore
					.add(transDensidad.get(i));
			histogramafored[i] = ((Double)(transDensidad.get(i).doubleValue() * 10000)).intValue();
		}
		promedioTransDensidadFore = sumTotalTransDensidadFore.divide(
				new BigDecimal(cantElementosFore), 20, RoundingMode.HALF_EVEN);

		BigDecimal promedioErrorTotalFore = promedioTransDensidadFore
				.subtract(promedioDensidadFore);

		sum = BigDecimal.ZERO;
		for (int i = threshold +1; i < 256; i++) {
			sum = transDensidad.get(i).add(promedioErrorTotalFore);
			transDensidad.add(i, sum);
			sum = BigDecimal.ZERO;
		}
		
		
		if (showGraphicDetails){
			GraphicsHelper.showHistogramFrame(thresholder, histograma, histogramabackd, histogramafored);
		}
		
		
		double[][] imagenTransformada = new double[width][height];
		BigDecimal acumulado = BigDecimal.ZERO;
		double valor;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (matrizThreshold[i][j] == 0) {
					valor = imagenOtsus.getMatrizOriginal()[i][j];
					acumulado = BigDecimal.ZERO;
					for (int k = 0; k <= valor; k++) {
						acumulado = acumulado.add(transDensidad.get(k));
					}
					// logger.info("valor del acumuladob: " +
					// acumulado);
					imagenTransformada[i][j] = minNivelGris
							.add((new BigDecimal(threshold - minNivelGris.intValue()))
									.multiply(acumulado)).doubleValue();
			/*		logger.info("imagen nueva : \n" + i + ","+ j + " = " +  
							imagenTransformada[i][j]);*/
				} else {
					valor = imagenOtsus.getMatrizOriginal()[i][j];
					acumulado = BigDecimal.ZERO;
					for (int k = 0; k <= valor; k++) {
						acumulado = acumulado.add(transDensidad.get(k));
					}
					// logger.info("valor del acumuladof: " +
					// acumulado);
					imagenTransformada[i][j] = (threshold + 1)
							+ ((maxNivelGris.intValue() - (threshold + 1)) * acumulado
									.doubleValue());
					/*logger.info("imagen nueva : \n" + i + ","+ j + " = " +  
							imagenTransformada[i][j]);*/
				}

			}
		}
		
		
//		for (int i = 0; i < width; i++) {
//			for (int j = 0; j < height; j++) {
//					valor = matrizOriginal[i][j];
//					acumulado = BigDecimal.ZERO;
//					for (int k = 0; k <= valor; k++) {
//						acumulado = acumulado.add(transDensidad.get(k));
//					}
//					imagenTransformada[i][j] = BigDecimal.ZERO
//							.add((new BigDecimal(255 - 0))
//									.multiply(acumulado)).doubleValue();
//			}
//		}

	/*	logger.info("La suma de densidad del back es: " + sumTotalDensidadBack);
		logger.info("La maxima densidad del back es: " + maxDensidadBack);
		logger.info("El promedio de densidad del back es: "
				+ promedioDensidadBack);
		logger.info("La suma de transformacion de densidad del back es: "
				+ sumTotalTransDensidadBack);
		logger.info("El promedio de transformacion de densidad del back es: "
				+ promedioTransDensidadBack);

		logger.info("La suma de densidad del fore es: " + sumTotalDensidadFore);
		logger.info("La maxima densidad del back es: " + maxDensidadFore);
		logger.info("La cantidad de elementos del fore es: "
				+ (cantElementosFore));
		logger.info("El promedio de densidad del fore es: "
				+ promedioDensidadFore);
		logger.info("La suma de transformacion de densidad del fore es: "
				+ sumTotalTransDensidadFore);
		logger.info("El promedio de transformacion de densidad del fore es: "
				+ promedioTransDensidadFore);*/

		BufferedImage nuevaImagen = new BufferedImage(width, height,
				BufferedImage.TYPE_BYTE_GRAY);

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int rgb = (int) imagenTransformada[i][j]<< 16 | (int) imagenTransformada[i][j]<< 8 | (int) imagenTransformada[i][j] ;
				// byte m = (byte)rgb ;
				nuevaImagen.setRGB(i, j, rgb);
				// logger.info("valor de la imagen en el pixel %d,%d imag transformada: "
				// +
				// i+j+imagenTransformada[i][j]);
			}
		}
		
		Imagen nuevaImagenOtsu = new Imagen(nuevaImagen);
		
		return nuevaImagenOtsu;
	}

	private void imprimirFrame(String ruta, String nombreImagen,
			BufferedImage nuevaImagen, Imagen imagenOtsus) {

		String formato = "png";

		File fichero = new File(ruta + nombreImagen + "_mohe." + formato);
		Raster raster = nuevaImagen.getData();
		DataBuffer buffer = raster.getDataBuffer();
		// DataBufferInt intBuffer = (DataBufferInt) buffer;
		DataBufferByte byteBuffer = (DataBufferByte) buffer;

		// int[] srcDataN = intBuffer.getData(0);
		byte[] scrDAtaB = byteBuffer.getData(0);
		try {
			ImageIO.write(nuevaImagen, formato, fichero);
		} catch (IOException e) {
			System.out.println("Error de escritura");
		}

		// Create GUI
		GreyFrame srcFrame = new GreyFrame(imagenOtsus.getWidth(),
				imagenOtsus.getHeight(), imagenOtsus.getSrcData());
		GreyFrame dstFrame = new GreyFrame(imagenOtsus.getWidth(),
				imagenOtsus.getHeight(), imagenOtsus.getDstData());
		GreyFrame improvedFrame = new GreyFrame(imagenOtsus.getWidth(),
				imagenOtsus.getHeight(), scrDAtaB);
		GreyFrame histFrame = GraphicsHelper.createHistogramFrame(thresholder);
		GreyFrame histFrame2 = GraphicsHelper.createHistogramFrame(thresholder
				.calcularHistograma(scrDAtaB), thresholder);
		
		

		JPanel infoPanel = new JPanel();
		infoPanel.add(histFrame);
		JPanel infoPanel2 = new JPanel();
		infoPanel2.add(histFrame2);

		GraphicsHelper.showHistogramFrame(infoPanel, srcFrame, improvedFrame, infoPanel2);
		
		// Save Images
		try {
			String basename = ruta + nombreImagen;

			javax.imageio.ImageIO.write(dstFrame.getBufferImage(), "PNG",
					new File(basename + "_BW.png"));
			javax.imageio.ImageIO.write(histFrame.getBufferImage(), "PNG",
					new File(basename + "_hist.png"));
			javax.imageio.ImageIO.write(improvedFrame.getBufferImage(), "PNG",
					new File(basename + "_mohe.png"));

		} catch (IOException ioE) {
			System.err.println("Could not write image " + ruta);
		}

	}

	

	

	public void compute(String imagen, String formato, Variable[] variables)
			throws JMException {
		String ruta = "rsc/images/baseImages/";
		Imagen imagenOtsus = new Imagen(ruta + imagen + "." + formato);

		Imagen nuevaImagen = mohe(imagenOtsus, new BigDecimal(
				variables[0].getValue()),
				new BigDecimal(variables[1].getValue()), new BigDecimal(
						variables[2].getValue()),
				new BigDecimal(variables[3].getValue()));

		imprimirFrame(ruta, imagen, nuevaImagen.getSrcImage(), imagenOtsus);

	}
	
	public static void main(String[] args) {
		Mohe moheNew = new Mohe();
		String ruta = "rsc/images/baseImages/";
		Imagen imagenOtsus = new Imagen(ruta + "pirate_original" + "." + "png");

		Imagen nuevaImagen = moheNew.mohe(imagenOtsus, new BigDecimal(
				0.5),
				new BigDecimal(0.5), new BigDecimal(
						0.5),
				new BigDecimal(0.5));

		moheNew.imprimirFrame(ruta, "lena_dark", nuevaImagen.getSrcImage(), imagenOtsus);
	}

}
