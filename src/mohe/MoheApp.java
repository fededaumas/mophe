package mohe;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import org.apache.log4j.Logger;

import jmetal.MoheJmetalImpl;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.problems.ProblemaMohe;
import jmetal.util.JMException;

public class MoheApp {
	
	public static int cantidadParticulas;
    public static int tamanhoArchivo;
    public static int cantidadIteraciones;
    public static String nombreImagen;
    public static String formatoImagen;
    
    private static Logger logger = Logger.getLogger("MoheApp");
	
	static {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("rsc/files/mohe_pso.properties");

			// load a properties file
			prop.load(input);

			// get the property value
			cantidadParticulas = Integer.valueOf(prop
					.getProperty("algoritmo.cantidadparticulas"));
			tamanhoArchivo = Integer.valueOf(prop
					.getProperty("algoritmo.tamanhoArchivo"));
			cantidadIteraciones = Integer.valueOf(prop
					.getProperty("algoritmo.cantidaditeraciones"));
			
			input.close();
			input = new FileInputStream("rsc/files/image_file.properties");

			// load a properties file
			prop.load(input);

			
			nombreImagen = prop.getProperty("nombreimagen");
			formatoImagen = prop.getProperty("formato");
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws ClassNotFoundException, JMException {
		
//		ProblemaMoheConThreshold problemaMohe = new ProblemaMoheConThreshold(nombreImagen,formatoImagen);
		ProblemaMohe problemaMohe = new ProblemaMohe(nombreImagen,formatoImagen);
		MoheJmetalImpl moheJmetal = new MoheJmetalImpl(problemaMohe);
		
		
        moheJmetal.setInputParameter("C1Max", 4.0);
        moheJmetal.setInputParameter("C1Min", 0.0);
        
        moheJmetal.setInputParameter("C2Max", 4.0);
        moheJmetal.setInputParameter("C2Min", 0.0);
        
		moheJmetal.setInputParameter("WMax", 0.9);
		moheJmetal.setInputParameter("WMin", 0.1);
		
		
		moheJmetal.setInputParameter("swarmSize", cantidadParticulas);
		moheJmetal.setInputParameter("archiveSize", tamanhoArchivo);
		moheJmetal.setInputParameter("maxIterations", cantidadIteraciones);
		
		HashMap parameters = new HashMap();//Operator parameters
        parameters.put("probability", 1.0 / problemaMohe.getNumberOfVariables());//para mutacion
        parameters.put("distributionIndex", 20.0);//para mutacion
        
		
		Mutation mutacion = MutationFactory.getMutationOperator("PolynomialMutation", parameters );//operador de turbulencia (mutacion)
		moheJmetal.addOperator("mutation", mutacion);
		
		
		long initTime = System.currentTimeMillis();
        SolutionSet poblacion = moheJmetal.execute();//ejecutar el pso
        long estimatedTime = System.currentTimeMillis() - initTime;
        logger.info("Tiempo total: " + estimatedTime + " ms");
        
        
        Mohe mohe = new Mohe();
		Iterator<Solution> i = poblacion.iterator();
		poblacion.printFeasibleFUN("rsc/files/MOHEFUN.txt");
		poblacion.printFeasibleVAR("rsc/files/MOHEVAR.txt");
		
		
		while(i.hasNext()) {
	         Solution sol = i.next();
	         Variable[] variables =  sol.getDecisionVariables();
	         mohe.compute(nombreImagen,formatoImagen, variables);
	         
//	         logger.info("Variables solucion: " + variables[0].getValue() + ", "
//	        		 + variables[1].getValue() + ", " + variables[2].getValue() + ", "
//	        		 + variables[3].getValue());
//	         
//	         logger.info(variables.length==5?"Threshold: "+new Double(variables[4].getValue()).intValue():"");
//	         
//	         logger.info("Entropia Solucion" + ": " + sol.getObjective(0));
//	         logger.info("Contraste Solucion" + ": " + sol.getObjective(1));
	         
	      }
		
	}

}

