package jmetal.problems;

import ij.process.ByteProcessor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import mohe.Mohe;
import otsus.OtsuThresholder;
import utils.Imagen;
import utils.ProblemUtils;

public class ProblemaMohe extends Problem {

	private static Logger logger = Logger.getLogger("");

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Mohe objetoMohe;
	Imagen imagenOtsus;
	BigDecimal promedioBackOriginal;
	BigDecimal promedioForeOriginal;
	BigDecimal promedioBackNuevo;
	BigDecimal promedioForeNuevo;
	OtsuThresholder thresholderO;
	
	
	List<Imagen> imagenesNuevas = new ArrayList<Imagen>();
	

	public ProblemaMohe(String imageFileName, String imageFormat) {
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";

		imagenOtsus = new Imagen(ruta + imagen + "." + formato);
		
		thresholderO = new OtsuThresholder();
		thresholderO.doThreshold(imagenOtsus.getSrcData(),
				imagenOtsus.getDstData());

		objetoMohe = new Mohe();

		numberOfVariables_ = 4;
		numberOfObjectives_ = 2;
		problemName_ = "moheProblema";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		lowerLimit_[0] = 0.1;
		upperLimit_[0] = 1;
		lowerLimit_[1] = 0.1;
		upperLimit_[1] = 1;
		lowerLimit_[2] = 0.1;
		upperLimit_[2] = 1;
		lowerLimit_[3] = 0.1;
		upperLimit_[3] = 1;

		solutionType_ = new RealSolutionType(this);
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] variables = solution.getDecisionVariables();
		double[] fx = new double[4];
		fx[0] = variables[0].getValue();
		fx[1] = variables[1].getValue();
		fx[2] = variables[2].getValue();
		fx[3] = variables[3].getValue();

		Imagen nuevaImagen = objetoMohe.mohe(imagenOtsus, new BigDecimal(
				fx[0]), new BigDecimal(fx[1]), new BigDecimal(fx[2]),
				new BigDecimal(fx[3]));
		
		
		ByteProcessor imagenOriginal = new ByteProcessor(imagenOtsus.getSrcImage());
		ByteProcessor imagenNueva = new ByteProcessor(nuevaImagen.getSrcImage());
		solution.setObjective(0, - ProblemUtils.calcularEntropia(nuevaImagen.getHistograma(), nuevaImagen.getCantPixeles()));
		solution.setObjective(1,- ProblemUtils.calcularSSIM(imagenOriginal, imagenNueva));

	}

	

}
