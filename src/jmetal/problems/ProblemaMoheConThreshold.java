package jmetal.problems;

import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import mohe.Mohe;
import otsus.OtsuThresholder;
import utils.Imagen;
import utils.ProblemUtils;

public class ProblemaMoheConThreshold extends Problem {

	private static Logger logger = Logger.getLogger("ProblemaMoheConThreshold");

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width;
	private int height;
	private Mohe objetoMohe;
	Imagen imagenOtsus;
	BigDecimal promedioBackOriginal;
	BigDecimal promedioForeOriginal;
	BigDecimal promedioBackNuevo;
	BigDecimal promedioForeNuevo;
	List<BigDecimal> promedio;
	private int[] histogramaOriginal;
	private OtsuThresholder thresholderO;
	

	public ProblemaMoheConThreshold(String imageFileName, String imageFormat) {
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";

		imagenOtsus = new Imagen(ruta + imagen + "." + formato);
		width = imagenOtsus.getWidth();
		height = imagenOtsus.getHeight();

		histogramaOriginal = new int[256];
		thresholderO = new OtsuThresholder();

		histogramaOriginal  = thresholderO.calcularHistograma(imagenOtsus.getSrcData());
		objetoMohe = new Mohe();
		

		numberOfVariables_ = 5;
		numberOfObjectives_ = 2;
		problemName_ = "moheProblema";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		lowerLimit_[0] = 0.1;
		upperLimit_[0] = 1;
		lowerLimit_[1] = 0.1;
		upperLimit_[1] = 1;
		lowerLimit_[2] = 0.1;
		upperLimit_[2] = 1;
		lowerLimit_[3] = 0.1;
		upperLimit_[3] = 1;
		lowerLimit_[4] = 1;
		upperLimit_[4] = 255;

		solutionType_ = new RealSolutionType(this);
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		// TODO Auto-generated method stub
		Variable[] variables = solution.getDecisionVariables();
		double[] fx = new double[5];
		fx[0] = variables[0].getValue();
		fx[1] = variables[1].getValue();
		fx[2] = variables[2].getValue();
		fx[3] = variables[3].getValue();
		fx[4] = variables[4].getValue();

		
		int threshold = new Double(fx[4]).intValue();
		Imagen nuevaImagen = objetoMohe.mohe(imagenOtsus, new BigDecimal(
				fx[0]), new BigDecimal(fx[1]), new BigDecimal(fx[2]),
				new BigDecimal(fx[3]), threshold, false);
		int[] histogramaNuevo;
		
		threshold = objetoMohe.getThreshold();
		
		OtsuThresholder thresholderNuevo = new OtsuThresholder();
		histogramaNuevo = thresholderNuevo.calcularHistograma(nuevaImagen.getSrcData());

		promedioBackOriginal = ProblemUtils.calcularPromedio(histogramaOriginal, threshold, 0);
		promedioForeOriginal = ProblemUtils.calcularPromedio(histogramaOriginal, threshold, 1);
		
		promedioBackNuevo = ProblemUtils.calcularPromedio(histogramaNuevo, threshold, 0);
		promedioForeNuevo = ProblemUtils.calcularPromedio(histogramaNuevo, threshold, 1);
		
		double entropia = ProblemUtils.calcularEntropia(histogramaOriginal, width * height);
		double entropiaNueva = ProblemUtils.calcularEntropia(histogramaNuevo, width * height);
		
		BigDecimal contraste = ProblemUtils.calcularContraste(promedioBackOriginal, promedioForeOriginal);
		BigDecimal contrasteNuevo = ProblemUtils.calcularContraste(promedioBackNuevo, promedioForeNuevo);



		logger.info("Solution --> Entropia Original: " + entropia);
		logger.info("Solution --> Contraste Original : " + contraste);

		logger.info("Solution --> Entropia Nueva: " + entropiaNueva);
		logger.info("Solution --> Contraste Nuevo : " + contrasteNuevo);

		logger.info("Solution --> Entropia Objetivo (EO - EN): "
				+ (entropia - entropiaNueva));
		logger.info("Solution --> Contraste Nuevo (CO - CN) : "
				+ (contraste.subtract(contrasteNuevo).doubleValue()));

		solution.setObjective(0, (-entropia - -entropiaNueva));
		solution.setObjective(1,
				(contraste.subtract(contrasteNuevo).doubleValue()));
		solution.setObjective(1, 0);

	}

	

}
