package mohe2;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import ij.plugin.histogram2.Util;
import ij.process.AutoThresholder;
import ij.process.AutoThresholder.Method;
import ij.process.ByteProcessor;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import utils.Imagen;
import utils.ProblemUtils;

public class MoheEqualizer extends Problem {
	
	private Imagen imagenOriginal;
	private List<Imagen> imagenesNuevas = new ArrayList<Imagen>();
	private String path;
	private boolean withThreshold = false;
	private boolean diferentesBinarizaciones = false;
	
	
	//Problem Variables
	private Double alfa1, alfa2, alfa3;
	
	private Method thresholderMethod = Method.Otsu; //MOHE DEFAULTS

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Logger logger = Logger.getLogger(MoheEqualizer.class);
	
	
	public static int cantidadParticulas;
    public static int tamanhoArchivo;
    public static int cantidadIteraciones;
    public static String nombreImagen;
    public static String formatoImagen;
    
    
	
	private void addProperties(String basePath) {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(basePath+"/mohe_pso.properties");

			// load a properties file
			prop.load(input);

			// get the property value
			cantidadParticulas = Integer.valueOf(prop
					.getProperty("algoritmo.cantidadparticulas"));
			tamanhoArchivo = Integer.valueOf(prop
					.getProperty("algoritmo.tamanhoArchivo"));
			cantidadIteraciones = Integer.valueOf(prop
					.getProperty("algoritmo.cantidaditeraciones"));
			
			input.close();
			input = new FileInputStream(basePath+"/image_file.properties");

			// load a properties file
			prop.load(input);

			
			nombreImagen = prop.getProperty("nombreimagen");
			formatoImagen = prop.getProperty("formato");
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}	
	
	
	public MoheEqualizer(Imagen imagenOriginal, boolean withThreshold ){
		addProperties("");
		this.imagenOriginal = imagenOriginal;
		setPSOParameters();
	}
	
	public MoheEqualizer(String basePath, String imageFileName, String imageFormat, boolean withThreshold ) {
		this(basePath,null, imageFileName, imageFormat,withThreshold);
	}
	
	
	public MoheEqualizer(String basePath, String path, String imageFileName, String imageFormat, boolean withThreshold) {
		addProperties("rsc/files/");
		this.path = path!=null?path:basePath;
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";

		this.imagenOriginal = new Imagen(ruta + imagen + "." + formato);
		this.withThreshold = withThreshold;
		setPSOParameters();
	}
	
	//overrides file specification
	public MoheEqualizer(String basePath ,String path, String imageFileName, String imageFormat, boolean withThreshold, Integer cantParticulas, Integer tamanhoSalida,
			Integer cantIteraciones) {
//		addProperties(basePath);
		cantidadParticulas = cantParticulas;
		tamanhoArchivo = tamanhoSalida;
		cantidadIteraciones = cantIteraciones;
		this.path = path;
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = basePath+"/baseImages/";

		this.imagenOriginal = new Imagen(ruta + imagen + "." + formato);
		this.withThreshold = withThreshold;
		setPSOParameters();
		
	}
	
	public MoheEqualizer(String path, String imageFileName, String imageFormat, boolean withThreshold, Double alfa1, Double alfa2,
			Double alfa3) {
		addProperties("");
		this.path = path;
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";

		this.imagenOriginal = new Imagen(ruta + imagen + "." + formato);
		this.withThreshold = withThreshold;
		setPSOParameters();
		this.alfa1= alfa1;
		this.alfa2= alfa2;
		this.alfa3= alfa3;
	}

	private int getVariablesNumber(){
		if (this.diferentesBinarizaciones){
			return 6;
		}
		if (this.withThreshold){
			return 5;
		}
		return 4;
	}
	
	private void setPSOParameters(){
		numberOfVariables_ = getVariablesNumber();
		numberOfObjectives_ = 1;
		problemName_ = "moheProblema";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		lowerLimit_[0] = 0.1;
		upperLimit_[0] = 1;
		lowerLimit_[1] = 0.1;
		upperLimit_[1] = 1;
		lowerLimit_[2] = 0.1;
		upperLimit_[2] = 1;
		lowerLimit_[3] = 0.1;
		upperLimit_[3] = 1;
		if (withThreshold){ 
			lowerLimit_[4] = imagenOriginal.getMinNivelGris() +1;
			upperLimit_[4] = imagenOriginal.getMaxNivelGris() -1;
		}
		if (diferentesBinarizaciones){
			lowerLimit_[5] = 0;
			upperLimit_[5] = Method.values().length-1;
		}

		solutionType_ = new RealSolutionType(this);
	}
	
	

	@Override
	public void evaluate(Solution solution) throws JMException {
		
		Variable[] variables = solution.getDecisionVariables();
		double[] fx = new double[variables.length];
		for (int i = 0; i<variables.length; i++ ){
			fx[i] = variables[i].getValue();
		}
		
		Double entropiaOriginal = ProblemUtils.calcularEntropia(this.imagenOriginal.getHistograma(), this.imagenOriginal.getCantPixeles());
		Double mediaOriginal = this.imagenOriginal.getMedia();
//		Double contrasteOriginal = this.imagenOriginal.getContraste();
		 
		
		Double sumaPonderada=null;
		
		Imagen nuevaImagen = mohe(fx);
		
		Double entropiaNueva = ProblemUtils.calcularEntropia(nuevaImagen.getHistograma(), nuevaImagen.getCantPixeles());
		Double mediaNueva = nuevaImagen.getMedia();
//		Double contrasteNuevo = nuevaImagen.getContraste();
		Double ssim = ProblemUtils.calcularSSIM(this.imagenOriginal, nuevaImagen);
//		Double psnr = ProblemUtils.calcularPSNR(this.imagenOriginal, nuevaImagen);
		// abs para que la resta tienda a 0 --> Mantener el brillo original.
		sumaPonderada = (entropiaOriginal-entropiaNueva)+Math.abs((mediaOriginal-mediaNueva));
		
		// NORMALIZAR VALORES
//		Double entropia = entropiaNueva/8;
//		Double contraste = contrasteNuevo/128;
		
//		sumaPonderada = -( (alfa1 * ssim )+ (alfa2 * entropia) + (alfa3 * contraste)  ); 
			
		solution.setObjective(0, sumaPonderada);
		
//		solution.setObjective(0, -ssim);
//		solution.setObjective(1, -entropiaNueva);
//		solution.setObjective(2, -(contrasteNuevo-contrasteOriginal));
//		solution.setObjective(3, -(psnr));
			
			
			
		

	}
	
	

	
	public void equalize() {
		try {
			MoheApp2 pso = new MoheApp2(this);

			pso.setInputParameter("swarmSize", cantidadParticulas);
			pso.setInputParameter("archiveSize", tamanhoArchivo); // MOHE is 1
			pso.setInputParameter("maxIterations", cantidadIteraciones);

			HashMap parameters = new HashMap();// Operator parameters
			parameters.put("probability", 1.0 / getNumberOfVariables());// para
																		// mutacion
			parameters.put("distributionIndex", 20.0);// para mutacion

			Mutation mutacion;
				mutacion = MutationFactory.getMutationOperator(
						"PolynomialMutation", parameters);
			// operador de turbulencia
														// (mutacion)
			pso.addOperator("mutation", mutacion);

			long initTime = System.currentTimeMillis();
			SolutionSet poblacion = pso.execute();// ejecutar el pso
			long estimatedTime = System.currentTimeMillis() - initTime;
			
//			FastHypervolume hv = new FastHypervolume();
//			double hyperVolume = hv.computeHypervolume(poblacion);
			
			logger.info("Tiempo total: " + estimatedTime + " ms");

			Iterator<Solution> iterator = poblacion.iterator();
			
			poblacion.printFeasibleFUN(this.path!=null?this.path + "/MOHEFUN.txt" : "rsc/files/MOHEFUN.txt");
			poblacion.printFeasibleVAR(this.path!=null?this.path + "/MOHEVAR.txt" : "rsc/files/MOHEVAR.txt");
			PrintWriter pw = new PrintWriter(this.path!=null?this.path + "/MOHENOBJECTIVES.txt" : "rsc/files/MOHENOBJECTIVES.txt");
			pw.close();
			try {
				
				BufferedWriter bw = new BufferedWriter(new FileWriter(this.path!=null?this.path + "/MOHENOBJECTIVES.txt" : "rsc/files/MOHENOBJECTIVES.txt", true));
//				bw.append("HiperVolumen:,"+hyperVolume+",,,");
				bw.newLine();
				bw.append("IMAGEN,CONTRASTE,ENTROPIA,SSIM,");
				bw.newLine();
				int j=0;
				while (iterator.hasNext()) {
					j++;
					Solution sol = iterator.next();
					Variable[] variables = sol.getDecisionVariables();
					double[] fx = new double [variables.length];
					for (int i = 0; i<variables.length; i++){
						fx[i] = variables[i].getValue();
					}
					Imagen imangenNueva = mohe(fx);
					imagenesNuevas.add(imangenNueva);
					
					double contraste = ProblemUtils.calcularContraste2(imangenNueva);
					double ssim = ProblemUtils.calcularSSIM(this.imagenOriginal, imangenNueva);
					double entropia = ProblemUtils.calcularEntropia(imangenNueva.getHistograma(), imangenNueva.getCantPixeles());
					
					
					
					bw.append( j+","+contraste+ "," 
							+ entropia + "," 
							+ ssim + ",");
					bw.newLine();
					
				}
				bw.close();
			} catch (IOException e) {
				Configuration.logger_.severe("Error accesing to the file");
				e.printStackTrace();
			}
				
			
			
		}catch(Exception e){
			logger.error("Error", e);
		}
	}
		
	// recieve a,b,c,d and t* if fx longer than 4
	public Imagen mohe(double[] fx) {
		try{
			//Threshold setting
			int threshold, cantPixelBack = 0, cantPixelFore = 0;
			int [] histograma = imagenOriginal.getHistograma();
			AutoThresholder at = new AutoThresholder();
			
			if (fx.length > 4){ // threshold given
				threshold = (int)Math.round(fx[4]);
			}else{
				threshold = at.getThreshold(thresholderMethod, histograma);
			}
			
			int [] histogramaB = new int[threshold+1];
			int [] histogramaF = new int[255-threshold];
			
			for (int i = 0; i < 256; i++){
				if(i<=threshold){ 
					cantPixelBack += histograma[i];
					histogramaB[i] = histograma[i];
				}else{
					cantPixelFore += histograma[i];
					histogramaF[i-(threshold+1)] = histograma[i];
				}
			}
			
			DensidadProperties dpB= ProblemUtils.getDensidadProperties(histogramaB);
			DensidadProperties dpF= ProblemUtils.getDensidadProperties(histogramaF);
			
			Double alfa = dpB.getMaxDensidad() * fx[1];
			Double beta = 0.0001;
			Double delta = dpF.getMaxDensidad() * fx[3];
			Double tita = dpF.getPromedioDensidad();
			
			
			double [] tpdfB = new double[threshold+1];
			double [] tpdfF = new double[255-threshold];
			double sumTpdfB = 0, sumTpdfF = 0;
			for (int i = 0; i < 256; i++){
				if(i<=threshold){
					//BACKGROUND SIDE
					if (dpB.getHistDensidad()[i] > alfa){
						tpdfB[i] = alfa;
					}else if (beta <= dpB.getHistDensidad()[i] && dpB.getHistDensidad()[i] <= alfa){
						tpdfB[i] = Math.pow( ((dpB.getHistDensidad()[i] - beta)/ (alfa - beta)) , fx[0]) * alfa; 
					}else{
						tpdfB[i] = 0;
					}
					sumTpdfB += tpdfB[i];
				}else{
					//FOREGROUND SIDE
					int k = i-(threshold+1);
					if (dpF.getHistDensidad()[k] > delta){
						tpdfF[k] = delta;
					}else if (tita <= dpF.getHistDensidad()[k] && dpF.getHistDensidad()[k] <= delta){
						tpdfF[k] = Math.pow( ((dpF.getHistDensidad()[k] - tita)/ (delta - tita)) , fx[2]) * delta; 
					}else{
						tpdfF[k] = tita;
					}
					sumTpdfF += tpdfF[k];
				}
			}
			
			for (int i = 0; i < 256; i++){
				if(i<=threshold){
					tpdfB[i]+= Math.abs((sumTpdfB/(threshold+1) - dpB.getPromedioDensidad()));
				}else{
					int k = i-(threshold+1);
					tpdfF[k] += Math.abs((sumTpdfF/(255-threshold) - dpF.getPromedioDensidad()));
				}
			}
			double[] cpdfB = Util.CdfFromPdf(tpdfB);
			double[] cpdfF = Util.CdfFromPdf(tpdfF);
			
			
			int[] moheH = new int[256]; 
			int [] histogramabackd = new int[256];
			int [] histogramafored = new int[256];
			for (int i = 0; i < 256; i++){
				if(i<=threshold){
					moheH[i] =(int) Math.round(0 + (
							(threshold - 0 ) *
							 cpdfB[i]
							));
					histogramabackd[i] = new Double(tpdfB[i] * 100000).intValue();
				}else{
					//FOREGROUND SIDE
					int k = i-(threshold+1);
					moheH[i] =(int) Math.round((threshold + 1) + (
							(255 - (threshold + 1) ) *
							 cpdfF[k]
							));
					
					
					histogramafored[i] = new Double(tpdfF[k] * 100000).intValue() ;
				}
			}
//			GraphicsHelper.showHistogramFrame(imagenOriginal, histograma, histogramabackd, histogramafored);
			
			ByteProcessor imagenOriginalJ = (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
			imagenOriginalJ.resetRoi();
			imagenOriginalJ.applyTable(moheH); 
			
			Imagen imagenNueva = new Imagen(imagenOriginalJ.getBufferedImage());
			
			
			
	//		int[][] matrizThreshold = imagenOriginal.getMatrizThreshold(threshold);
	//		int[][] imagenTransformada = new int[imagenOriginal.getWidth()][imagenOriginal.getHeight()];
	//		int valor;
	//		BufferedImage nuevaImagen = new BufferedImage(imagenOriginal.getWidth(), imagenOriginal.getHeight(),
	//				BufferedImage.TYPE_BYTE_GRAY);
	//		for (int i = 0; i < imagenOriginal.getWidth(); i++) {
	//			for (int j = 0; j < imagenOriginal.getHeight(); j++) {
	//				valor = imagenOriginal.getMatrizOriginal()[i][j];
	//				if (valor <= threshold){
	//					imagenTransformada[i][j] =(int) Math.round(0 + (
	//							(threshold - 0 ) *
	//							cpdfB[valor]
	//							));
	//				}else{
	//					imagenTransformada[i][j] = (int) Math.round((threshold + 1) + (
	//							(255 - (threshold + 1) ) *
	//							cpdfF[valor-(threshold+1)]
	//							));
	//				}
	//				int rgb = (int) imagenTransformada[i][j]<< 16 | (int) imagenTransformada[i][j]<< 8 | (int) imagenTransformada[i][j] ;
	//				nuevaImagen.setRGB(i, j, rgb);
	//			}
	//		}
	//		Imagen imagenNueva = new Imagen(nuevaImagen);
			return imagenNueva;
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error", e);
			return null;
		}
	}

	public static void main(String[] args) throws Exception{
		String ruta = "rsc/images/baseImages/";
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("rsc/files/image_file.properties");

			// load a properties file
			prop.load(input);

			nombreImagen = prop.getProperty("nombreimagen");
			formatoImagen = prop.getProperty("formato");
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		MoheEqualizer equalizer = new MoheEqualizer("rsc/files/","/rsc/files",nombreImagen , formatoImagen,false);
//		equalizer.equalize();
		double [] fx = {0.5,0.5,0.5,0.5};
		Imagen imagenNueva = equalizer.mohe(fx);
		
//		GraphicsHelper.imprimirFrame("C:/tmp/", nombreImagen,
//				imagenNueva,
//				imagenNueva, "MOHE" + "Union");
		
		int i =0;
//		for (Imagen imagenNueva : equalizer.getImagenesNuevas()){
//			i++;
//				GraphicsHelper.imprimirFrame(ruta, nombreImagen,
//					equalizer.getImagenOriginal(),
//					imagenNueva, "MOHE" + i);
//			
//		}
	}

	public Imagen getImagenOriginal() {
		return imagenOriginal;
	}

	public void setImagenOriginal(Imagen imagenOriginal) {
		this.imagenOriginal = imagenOriginal;
	}

	public List<Imagen> getImagenesNuevas() {
		return imagenesNuevas;
	}

	public void setImagenesNuevas(List<Imagen> imagenesNuevas) {
		this.imagenesNuevas = imagenesNuevas;
	}

	public void setThresholderMethod(Method thresholderMethod) {
		this.thresholderMethod = thresholderMethod;
	}
}
