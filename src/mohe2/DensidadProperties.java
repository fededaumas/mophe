package mohe2;

public class DensidadProperties {
	private double [] histDensidad;
	private double maxDensidad;
	private double sumDensidad;
	private double promedioDensidad;
	private double promedio;
	
	
	public double[] getHistDensidad() {
		return histDensidad;
	}
	public void setHistDensidad(double[] histDensidad) {
		this.histDensidad = histDensidad;
	}
	public double getMaxDensidad() {
		return maxDensidad;
	}
	public void setMaxDensidad(double maxDensidad) {
		this.maxDensidad = maxDensidad;
	}
	public double getPromedioDensidad() {
		return promedioDensidad;
	}
	public void setPromedioDensidad(double promedioDensidad) {
		this.promedioDensidad = promedioDensidad;
	}
	public double getSumDensidad() {
		return sumDensidad;
	}
	public void setSumDensidad(double sumDensidad) {
		this.sumDensidad = sumDensidad;
	}
	public double getPromedio() {
		return promedio;
	}
	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}
	
	
	
}
