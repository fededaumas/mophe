/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otsus;

/**
 * 
 * @author diana
 */
public class OtsuThresholder {
	public int histData[];
	public int maxLevelValue;
	public int minLevelValue;
	

	public int threshold;
	public int wB;
	private float mB;
	private float mF;

	public int getwB() {
		return wB;
	}

	public int getwF() {
		return wF;
	}
	
	public float getMb(){
		return mB;
	}

	public float getMf(){
		return mF;
	}
	
	public int wF;

	public OtsuThresholder() {
		histData = new int[256];
		wB = 0;
		wF = 0;
	}

	public int[] getHistData() {
		return histData;
	}

	public int getMaxLevelValue() {
		return maxLevelValue;
	}
	
	public int getMinLevelValue() {
		return minLevelValue;
	}

	public int getThreshold() {
		return threshold;
	}

	public int[] calcularHistograma(byte[] srcData) {

		int ptr;
		int histograma[] = new int[256];
		// Clear histogram data
		// Set all values to zero
		ptr = 0;
		while (ptr < histograma.length)
			histograma[ptr++] = 0;

		// Calculate histogram and find the level with the max value
		// Note: the max level value isn't required by the Otsu method
		ptr = 0;
		maxLevelValue = 0;
		minLevelValue = 256;
		while (ptr < srcData.length) {
			int h = 0xFF & srcData[ptr];
			histograma[h]++;
			if (minLevelValue>histograma[h] && histograma[h] != 0){
				minLevelValue = histograma[h];
			}
			if (histograma[h] > maxLevelValue)
				maxLevelValue = histograma[h];
			ptr++;
		}
		histData = histograma ;
		return histograma;
	}

	public int doThreshold(byte[] srcData, byte[] monoData) {

		histData = calcularHistograma(srcData);
		// Total number of pixels
		int total = srcData.length;

		// Es el promedio.. 0*h1+1*h2........256*h256.. EL valos esperado
		float sum = 0;
		for (int t = 0; t < 256; t++)
			sum += t * histData[t];

		float sumB = 0;
		wB = 0;
		wF = 0;

		float varMax = 0;
		threshold = 0;

		for (int t = 0; t < 256; t++) {
			wB += histData[t]; // Weight Background

			if (wB == 0) {
				continue;
			}

			wF = total - wB; // Weight Foreground
			if (wF == 0)
				break;

			sumB += (float) (t * histData[t]);

			 mB = sumB / wB; // Mean Background
			 mF = (sum - sumB) / wF; // Mean Foreground

			// Calculate Between Class Variance
			float varBetween = (float) wB * (float) wF * (mB - mF) * (mB - mF);

			// Check if new maximum found
			if (varBetween > varMax) {
				varMax = varBetween;
				threshold = t;
			}
		}

		// Apply threshold to create binary image
		if (monoData != null) {
			int ptr = 0;
			while (ptr < srcData.length) {
				monoData[ptr] = ((0xFF & srcData[ptr]) >= threshold) ? (byte) 255: 0;
				ptr++; 
			}
		}

		return threshold;
	}
}