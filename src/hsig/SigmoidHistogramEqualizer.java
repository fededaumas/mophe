package hsig;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;

import ij.plugin.histogram2.HistogramMatcher;
import ij.process.ByteProcessor;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import utils.GraphicsHelper;
import utils.Imagen;
import utils.ProblemUtils;

public class SigmoidHistogramEqualizer extends Problem {

	Imagen imagenOriginal;
	Imagen imagenNueva;
	private String path;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static int cantidadParticulas;
	public static int tamanhoArchivo;
	public static int cantidadIteraciones;
	public static String nombreImagen;
	public static String formatoImagen;
	public static int[][] coffMatrix;

	private static Logger logger = Logger.getLogger(SigmoidHistogramEqualizer.class);

	static {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("rsc/files/sig_pso.properties");

			// load a properties file
			prop.load(input);

			// get the property value
			cantidadParticulas = Integer.valueOf(prop
					.getProperty("algoritmo.cantidadparticulas"));
			tamanhoArchivo = Integer.valueOf(prop
					.getProperty("algoritmo.tamanhoArchivo"));
			cantidadIteraciones = Integer.valueOf(prop
					.getProperty("algoritmo.cantidaditeraciones"));
			
			BufferedReader br = new BufferedReader(new FileReader("rsc/files/coff.txt"));
			String line;
			int row=0;
			coffMatrix = new int[9][256];
			 while ((line = br.readLine()) != null) {
		            String[] vals = line.trim().split(",");
		            for (int col = 0; col < 256; col++) {
		            	coffMatrix[row][col] = Integer.parseInt(vals[col]);
		            }
		            row++;
			 }
			
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * Values defined on paper Automatic Contrast Enhancement Technology with
	 * Saliency Preservation Ke Gu, Guangtao Zhai, Member, IEEE, Xiaokang Yang,
	 * Senior Member, IEEE, Wenjun Zhang, Fellow, IEEE, and Chang Wen Chen,
	 * Fellow, IEEE
	 */
	public static final int[] A = { 0, 255, 128, 12 };
	public static final int[] B = { 0, 255, 128, 25 };

	public SigmoidHistogramEqualizer(String imageFileName, String imageFormat) {
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";
		imagenOriginal = new Imagen(ruta + imagen + "." + formato);
		setPSOVariables();
	}

	public SigmoidHistogramEqualizer(Imagen imagenOriginal) {

		this.imagenOriginal = imagenOriginal;
		setPSOVariables();
	}

	public SigmoidHistogramEqualizer(String path, String imageFileName, String imageFormat) {
		this.path = path;
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";
		imagenOriginal = new Imagen(ruta + imagen + "." + formato);
		setPSOVariables();
	}

	private void setPSOVariables() {
		numberOfVariables_ = 5;
		numberOfObjectives_ = 1;
		problemName_ = "moheProblema";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		lowerLimit_[0] = 0;
		upperLimit_[0] = 255;
		lowerLimit_[1] = 0;
		upperLimit_[1] = 255;
		lowerLimit_[2] = 0;
		upperLimit_[2] = 255;
		lowerLimit_[3] = 0;
		upperLimit_[3] = 255;
		lowerLimit_[4] = 0;
		upperLimit_[4] = 255;

		solutionType_ = new RealSolutionType(this);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Deprecated
	public void equalize2()  {
		
		try {
		SigmoidPSO pso = new SigmoidPSO(this);

		pso.setInputParameter("swarmSize", cantidadParticulas);
		pso.setInputParameter("archiveSize", tamanhoArchivo);
		pso.setInputParameter("maxIterations", cantidadIteraciones);

		HashMap parameters = new HashMap();// Operator parameters
		parameters.put("probability", 1.0 / getNumberOfVariables());// para
																	// mutacion
		parameters.put("distributionIndex", 20.0);// para mutacion

		Mutation mutacion;
			mutacion = MutationFactory.getMutationOperator(
					"PolynomialMutation", parameters);
		// operador de turbulencia
													// (mutacion)
		pso.addOperator("mutation", mutacion);

		long initTime = System.currentTimeMillis();
		SolutionSet poblacion = pso.execute();// ejecutar el pso
		long estimatedTime = System.currentTimeMillis() - initTime;
		logger.info("Tiempo total: " + estimatedTime + " ms");

		Iterator<Solution> iterator = poblacion.iterator();
		// Should only have one solution monoObjective
		if (iterator.hasNext()) {
			Solution sol = iterator.next();
			Variable[] variables = sol.getDecisionVariables();
			logger.info("Variables solucion (PIS): " + variables[0].getValue()
					+ ", " + variables[1].getValue() + ", "
					+ variables[2].getValue() + ", " + variables[3].getValue());

			double[] piOptimos = new double[4];
			piOptimos[0] = variables[0].getValue();
			piOptimos[1] = variables[1].getValue();
			piOptimos[2] = variables[2].getValue();
			piOptimos[3] = variables[3].getValue();

			logger.info("Pi OPT" + ": " + sol.getObjective(0));

			ByteProcessor imagenOriginalJ = new ByteProcessor(
					getImagenOriginal().getSrcImage());
			imagenOriginalJ.resetRoi();
			int[] hist = imagenOriginalJ.getHistogram();
			int[] newHist = new int[256];
			System.out.println(hist);

			for (int i = 0; i < 256; i++) {
				newHist[i] = (int)Math.round(Math.max(Math.min(SigmoidHistogramEqualizer.getIsig(hist[i], piOptimos), 255), 0));
			}
			HistogramMatcher matcher = new HistogramMatcher();
			imagenOriginalJ.applyTable(matcher.matchHistograms(hist, newHist));
			setImagenNueva(new Imagen(imagenOriginalJ.getBufferedImage()));

		}
		} catch (JMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void equalize(){
		
		ByteProcessor imagenOriginalJ =(ByteProcessor) new ByteProcessor(imagenOriginal.getSrcImage()).duplicate();
		ByteProcessor imagenOrig2= (ByteProcessor) new ByteProcessor(imagenOriginal.getSrcImage()).duplicate();
		imagenOriginalJ.resetRoi();
		int[] newHist = new int[256];
		
		for (int i = 0; i < 256; i++) {
			newHist[i] = coffMatrix[(int) (Math.ceil(imagenOriginal.getMedia()/32)+1)][i];
		}
		imagenOriginalJ.applyTable(newHist);
		setImagenNueva(new Imagen(imagenOriginalJ.getBufferedImage()));
		
		double entropy = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(),imagenNueva.getCantPixeles());
		double ssim = ProblemUtils.calcularSSIM(imagenOrig2,imagenOriginalJ);
		
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter(this.path!=null?this.path + "/HSigFUN.txt" : "rsc/files/HSigFUN.txt", false));
			bw.append(entropy + " " + ssim);
			bw.close();
		} catch (IOException e) {
			Configuration.logger_.severe("Error acceding to the file");
			e.printStackTrace();
		}
		
	}
	
	public Imagen getImagenOriginal() {
		return imagenOriginal;
	}

	public void setImagenOriginal(Imagen imagenOriginal) {
		this.imagenOriginal = imagenOriginal;
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] variables = solution.getDecisionVariables();
		double[] fx = new double[5];
		fx[0] = variables[0].getValue();
		fx[1] = variables[1].getValue();
		fx[2] = variables[2].getValue();
		fx[3] = variables[3].getValue();
		fx[4] = variables[4].getValue();

		double media = imagenOriginal.getMedia();
//		int A2 = (int) (((int) (media / 32) + 1) * 32);
		int A2 = (int) (Math.ceil(media/32)+1);

		solution.setObjective(0, encontrarMinPiOptimos(fx, A2));

	}

	public Imagen getImagenNueva() {
		return imagenNueva;
	}

	public void setImagenNueva(Imagen imagenNueva) {
		this.imagenNueva = imagenNueva;
	}

	public Double encontrarMinPiOptimos(double[] fx, int a2) {
		Double piOPT = 0D;
		A[2] = B[2] = a2;
		A[3] = (int)fx[4];
		for (int i = 0; i < 4; i++) {
			piOPT += Math.abs(A[i] - getIsig(B[i], fx));
		}
		return piOPT;
	}

	public static Double getIsig(int i, double[] pis) {
		Double iSig = ((pis[0] - pis[1]) / (1 + Math
				.exp(-((i - pis[2]) / pis[3])))) + pis[1];
		return iSig;
	}

	public static void main(String[] args) throws ClassNotFoundException,
			JMException {
		SigmoidHistogramEqualizer problemaSigmoid = new SigmoidHistogramEqualizer("lena_dark",
				"png");
		problemaSigmoid.equalize();
		String ruta = "rsc/images/baseImages/";
		GraphicsHelper.imprimirFrame(ruta, "lena_dark",
				problemaSigmoid.getImagenOriginal(),
				problemaSigmoid.getImagenNueva(), "HSIG");

		double entropy = ProblemUtils.calcularEntropia(problemaSigmoid.getImagenNueva().getHistograma(),problemaSigmoid.getImagenNueva().getCantPixeles());
		ByteProcessor imagenOriginalJ = new ByteProcessor(
				problemaSigmoid.getImagenOriginal().getWidth(),
				problemaSigmoid.getImagenOriginal().getHeight(),
				problemaSigmoid.getImagenOriginal().getSrcData());
		ByteProcessor imagenNuevaJ = new ByteProcessor(
				problemaSigmoid.getImagenOriginal().getWidth(),
				problemaSigmoid.getImagenOriginal().getHeight(),
				problemaSigmoid.getImagenNueva().getSrcData());
		double ssim = ProblemUtils.calcularSSIM(imagenOriginalJ, imagenNuevaJ);
		
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter("rsc/files/HSigFUN.txt", false));
			bw.append(entropy + " " + ssim);
			bw.close();
		} catch (IOException e) {
			Configuration.logger_.severe("Error acceding to the file");
			e.printStackTrace();
		}
	}

}
