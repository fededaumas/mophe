package he;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import ij.plugin.ContrastEnhancer;
import ij.plugin.histogram2.HistogramMatcher;
import ij.process.ByteProcessor;
import jmetal.util.Configuration;
import utils.GraphicsHelper;
import utils.Imagen;
import utils.ProblemUtils;

public class BaseHistogramEqualizer {
	
	private Imagen imagenOriginal;
	private Imagen imagenNueva;
	private List<BigDecimal> densidadO;
	private String path;
	
	public BaseHistogramEqualizer(Imagen imagenOriginal){
		this.imagenOriginal = imagenOriginal;
	}
	
	public BaseHistogramEqualizer(String filePath){
		this.imagenOriginal =  new Imagen(filePath);
	}
	
	public BaseHistogramEqualizer(String nombreImagen, String formato){
		String imagen = nombreImagen != null ? nombreImagen : "lena_dark";
		String ruta = "rsc/images/baseImages/";
		imagenOriginal = new Imagen(ruta + imagen + "." + formato);
	}
	
	public BaseHistogramEqualizer(String path, String nombreImagen, String formato){
		this.path = path;
		String imagen = nombreImagen != null ? nombreImagen : "lena_dark";
		String ruta = "rsc/images/baseImages/";
		imagenOriginal = new Imagen(ruta + imagen + "." + formato);
	}
	
	public Imagen getImagenOriginal() {
		return imagenOriginal;
	}
	
	
	public void setImagenOriginal(Imagen imagenOriginal) {
		this.imagenOriginal = imagenOriginal;
	}
	
	
	@Deprecated
	public void equalize1(){
		
		if (imagenOriginal!=null && imagenNueva==null){
			int width = imagenOriginal.getSrcImage().getWidth();
			int height = imagenOriginal.getSrcImage().getHeight();
			double[][] imagenTransformada = new double[width][height];
			BigDecimal acumulado = BigDecimal.ZERO;
			int valor;
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					valor = imagenOriginal.getMatrizOriginal()[i][j];
					acumulado = BigDecimal.ZERO;
					for (int k = 0; k <= valor; k++) {
						acumulado = acumulado.add(imagenOriginal.getDensidad().get(k));
					}
					imagenTransformada[i][j] = new BigDecimal(imagenOriginal.getMinNivelGris())
							.add((new BigDecimal( imagenOriginal.getMaxNivelGris() - imagenOriginal.getMinNivelGris()))
									.multiply(acumulado)).doubleValue();
					
				}

			}
			BufferedImage nuevaImagen = new BufferedImage(width, height,
					BufferedImage.TYPE_BYTE_GRAY);
			
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					int rgb = (int) imagenTransformada[i][j]<< 16 | (int) imagenTransformada[i][j]<< 8 | (int) imagenTransformada[i][j] ;
					nuevaImagen.setRGB(i, j, rgb);
				}
			}
			imagenNueva= new Imagen(nuevaImagen);
			
		}
	}
	
	
	public void equalize(){
		//Invoque ImageJ HE
		ByteProcessor imagenOriginalJ = (ByteProcessor) new ByteProcessor(imagenOriginal.getSrcImage()).duplicate();
		ByteProcessor imagenOrig2= (ByteProcessor) new ByteProcessor(imagenOriginal.getSrcImage()).duplicate();
		imagenOriginalJ.resetRoi();
		ContrastEnhancer enhacer = new ContrastEnhancer();
//		int[] hi = imagenOriginalJ.getHistogram();
		enhacer.equalize(imagenOriginalJ);
//		int[] heq = imagenOriginalJ.getHistogram();
		
		imagenNueva = new Imagen(imagenOriginalJ.getBufferedImage());
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(this.path!=null?this.path + "/HEFUN.txt" : "rsc/files/HEFUN.txt", false));
			double entropy = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(),imagenNueva.getCantPixeles());
			double entropyOri = ProblemUtils.calcularEntropia(imagenOriginal.getHistograma(),imagenOriginal.getCantPixeles());
			double ssim = ProblemUtils.calcularSSIM(imagenOrig2,imagenOriginalJ);
			double contraste = imagenNueva.getContraste();
			bw.append("IMAGEN,CONTRASTE,ENTROPIA,SSIM,");bw.newLine();
			bw.append("Original," +imagenOriginal.getContraste() + "," + entropyOri + ",1" );bw.newLine();
			bw.append("HE," +contraste + "," + entropy + "," +ssim );
			bw.close();
		}catch(Exception e){
			System.out.println(e.getStackTrace());
		}
		
		
		
	}
	
	@Deprecated
	public void equalize2(){
		
		if (imagenOriginal!=null && imagenNueva==null){
			int width = imagenOriginal.getSrcImage().getWidth();
			int height = imagenOriginal.getSrcImage().getHeight();
			double[][] imagenTransformada = new double[width][height];
			Double acumulado = 0D;
			int valor;
			Double minFDAo = 0D;
			
			acumulado = 0D;
			int densidadK = 0;
			
			
			do{
				minFDAo = imagenOriginal.getFDA(densidadK);
				densidadK++;
			}while(minFDAo==0D);
			
			int [] fdaNuevo = new int [256];
			acumulado = 0D;
			for (int i = 0; i<256; i++){
				fdaNuevo[i] = new Double((imagenOriginal.getFDA(i) - minFDAo)*(255)/((width*height)- minFDAo)).intValue();
			}
			
			
			
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					valor = imagenOriginal.getMatrizOriginal()[i][j];
					imagenTransformada[i][j] = fdaNuevo[valor]; 
				}
			}
			
			BufferedImage nuevaImagen = new BufferedImage(width, height,
					BufferedImage.TYPE_BYTE_GRAY);
			
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					int rgb = (int) imagenTransformada[i][j]<< 16 | (int) imagenTransformada[i][j]<< 8 | (int) imagenTransformada[i][j] ;
					nuevaImagen.setRGB(i, j, rgb);
				}
			}
			imagenNueva= new Imagen(nuevaImagen);
			
		}
	}
	
	public Imagen getImagenNueva() {
		return imagenNueva;
	}


	public void setImagenNueva(Imagen imagenNueva) {
		this.imagenNueva = imagenNueva;
	}


	public static void main(String[] args) {
		String ruta = "rsc/images/baseImages/";
		BaseHistogramEqualizer he = new BaseHistogramEqualizer(ruta + "pirate_original" + "." + "png");

		he.equalize();
		GraphicsHelper.imprimirFrame(ruta, "pirate_original", he.getImagenOriginal(), he.getImagenNueva(), "HE");
		double entropy = ProblemUtils.calcularEntropia(he.getImagenNueva().getHistograma(),he.getImagenNueva().getCantPixeles());
		double mse = ProblemUtils.calcularMSE(he.getImagenOriginal(), he.getImagenNueva());
		double psnr = ProblemUtils.calcularPSNR(he.getImagenOriginal(), he.getImagenNueva());
		
		
		ByteProcessor imagenOriginalJ = new ByteProcessor(
				he.getImagenOriginal().getWidth(),
				he.getImagenOriginal().getHeight(),
				he.getImagenOriginal().getSrcData());
		HistogramMatcher matcher = new HistogramMatcher();
		imagenOriginalJ.resetRoi();
		ByteProcessor imagenNuevaJ = new ByteProcessor(
				he.getImagenOriginal().getWidth(),
				he.getImagenOriginal().getHeight(),
				he.getImagenNueva().getSrcData());
		double ssim = ProblemUtils.calcularSSIM(imagenOriginalJ, imagenNuevaJ);
		int lut [] = matcher.matchHistograms(imagenOriginalJ.getHistogram(), imagenNuevaJ.getHistogram());
		System.out.println(entropy + " " + ssim + " " + mse + " " + psnr );
		
		
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter("rsc/files/HPSOFUN.txt", false));
			
			bw.append(entropy + " " + ssim );
			bw.close();
			
			
			bw = new BufferedWriter(new FileWriter("rsc/files/INITIAL_PARETO.txt", true));
			String h = "";
//			int lut [] = matcher.matchHistograms(imagenOriginalJ.getHistogram(), imagenOriginalJ.getHistogram());
			for (int v : lut){
				h += v+" ";
			}
			
			entropy = ProblemUtils.calcularEntropia(he.getImagenOriginal().getHistograma(),he.getImagenNueva().getCantPixeles());
			
			bw.append(-entropy + " " + ssim + " " + h);
			bw.close();		

			
		} catch (IOException e) {
			Configuration.logger_.severe("Error acceding to the file");
			e.printStackTrace();
		}
		 

		
	}
	
	
	
	
}
