package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import asp.ASPApp;
import he.BaseHistogramEqualizer;
import hpso.HPSOEqualizer;
import hsig.SigmoidHistogramEqualizer;
import ij.process.AutoThresholder.Method;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import jmetal.util.Configuration;
import mohe2.MoheEqualizer;
import mohe2.MopheEqualizer;
import utils.GraphicsHelper;
import utils.Imagen;
import utils.MetodoEcualizacion;
import utils.ProblemUtils;
import utils.ResultsPropsHelper;

public class MainApp {
	
	private static Logger logger = Logger.getLogger(MainApp.class);

	public static void main(String[] args) throws FileNotFoundException {

		String algorithm = args[0];
		String baseImagesPath = args[1];
		String imagesFolder = baseImagesPath + "/baseImages/";
		
//		---------------------------------------------------------	
//		---------------------MOHE -----------------------------
//		---------------------------------------------------------
		logger.info("Trying to Execute algorithm: " + algorithm);
		if (algorithm.equalsIgnoreCase("moheO")) {
			File theDir = new File(baseImagesPath +"/moheO");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/moheO/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/moheO/" + i);
				i++;
			}

			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 1; j < listOfimages.length; j++) {
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();
					Calendar cal = Calendar.getInstance();
					MoheEqualizer equalizer = new MoheEqualizer(ruta,
							nombreImagen, formato, false);
					equalizer.equalize();
					Calendar calEnd = Calendar.getInstance();
					System.out.println("Executed in " + (calEnd.getTimeInMillis() - cal.getTimeInMillis()) + "ms");
					
					i = 0;
					for (Imagen imagenNueva : equalizer.getImagenesNuevas()) {
						i++;
						GraphicsHelper.saveToFolder(ruta,
								nombreImagen, formato,
								equalizer.getImagenOriginal(), imagenNueva,
								"" + i);

					}
					
					
					
					
					
				}

			}
//		---------------------------------------------------------	
//		---------------------MOPHE -----------------------------
//		---------------------------------------------------------	
		} else if (algorithm.equalsIgnoreCase("mophe")) {
				System.out.println("Running MOHE T with " + args[4] + " Iterations ");
				File theDir = new File(baseImagesPath +"/mophe");
				theDir.mkdirs();
				theDir = new File(baseImagesPath +"/mophe/0_"+args[4]);
				int i = 1;
				while (!theDir.mkdir()) {
					theDir = new File(baseImagesPath +"/mophe/" + i + "_"+args[4]);
					i++;
				}
	
				File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
				File[] listOfimages = baseImagesFolder.listFiles();
	
				for (int j = 0; j < listOfimages.length; j++) {
					
					String[] fileData = listOfimages[j].getName().split("\\.");
					String nombreImagen = fileData[0];
					String formato = fileData[1];
					
					
					if (listOfimages[j].isFile() && !formato.equalsIgnoreCase("DS_store")) {
						String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
						File fichero = new File(ruta);
						fichero.mkdir();
						MopheEqualizer equalizer = null;
						System.out.println("Running MOHE T for " + nombreImagen + " file ");
						Calendar cal = Calendar.getInstance();
						
						if (args.length >3){
							equalizer = new MopheEqualizer(baseImagesPath, ruta, nombreImagen, formato, true,
								Integer.valueOf(args[2]), Integer.valueOf(args[3]), Integer.valueOf(args[4]));
						}else{
							equalizer = new MopheEqualizer(baseImagesPath, ruta, nombreImagen, formato, true);
						}
						equalizer.equalize();
						Calendar calEnd = Calendar.getInstance();
						System.out.println("Executed in " + (calEnd.getTimeInMillis() - cal.getTimeInMillis()) + "ms");
						i = 0;
						/* LEGACY
//						double bestSumaConAmbe = 0;
//						int bestSumaConAmbeIndex = 0;
//						Imagen bestImagenSumaConAmbe  = null;
//						
//						double bestSSIM = 0;
//						Imagen bestImagenSSIM = null;
//						int bestSSIMIndex = 0;
//						
//						double bestSuma = 0;
//						Imagen bestImagenSuma = null;
//						int bestSumaIndex = 0;
//						
//						double bestSumaCons2 = 0;
//						Imagen bestImagenSumaCons2 = null;
//						int bestSumaCons2Index = 0;
 * */

						
						ResultsPropsHelper helper = new ResultsPropsHelper(equalizer.getImagenOriginal(), equalizer.getImagenesNuevas(), 
								utils.ResultsPropsHelper.Method.CLUSTERIZACION_SSIM, 10);
						List<Imagen>imagenesNuevasDominantes = helper // CRITERIO DE SELECCION 
								.getResultadosPorEstandarizacion();
						PrintWriter pw = new PrintWriter(ruta + "/MOHETOBJECTIVES.txt");
						pw.close();
						try {
							
							BufferedWriter bw = new BufferedWriter(new FileWriter(ruta + "/MOHETDOBJECTIVES.txt", true));
							bw.append("INDEX,CONTRASTE,ENTROPIA,SSIM,");
							bw.newLine();
						for (Imagen imagenNueva : equalizer.getImagenesNuevas()) {
							i++;
							if (imagenesNuevasDominantes.contains(imagenNueva)){
								double contraste = ProblemUtils.calcularContraste2(imagenNueva);
								double ssim = ProblemUtils.calcularSSIM(equalizer.getImagenOriginal(), imagenNueva);
								double entropia = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(), imagenNueva.getCantPixeles());
								
								
								
								bw.append( contraste+ "," 
										+ entropia + "," 
										+ ssim + ",");
								bw.newLine();
							
								GraphicsHelper.saveToFolder(ruta,
										nombreImagen, formato,
										equalizer.getImagenOriginal(), imagenNueva,
										 "Dominante_Cluster_"+ i);
							}else{
							GraphicsHelper.saveToFolder(ruta,
									nombreImagen, formato,
									equalizer.getImagenOriginal(), imagenNueva,
									 ""+ i);
							}
	
						}
						bw.close();
					} catch (IOException e) {
						Configuration.logger_.severe("Error accesing to the file");
						e.printStackTrace();
					} 
/* LEGACY
 * 						Double entropiaNueva = ProblemUtils.calcularEntropia(imagenNueva.getHistograma(), imagenNueva.getCantPixeles()/8);
//						double ssim = ProblemUtils.calcularSSIM(equalizer.getImagenOriginal(), imagenNueva);
//						double ambe = Math.abs(equalizer.getImagenOriginal().getMedia() - imagenNueva.getMedia());
//						Double contrasteNuevo = imagenNueva.getContraste()/128;
//						
//						Double suma = entropiaNueva + ssim + contrasteNuevo; 
//						Double sumaConAmbe = entropiaNueva + ssim + contrasteNuevo + ambe;
//						
//						Double sumaContDiv2 = entropiaNueva + ssim + contrasteNuevo/2; 
//						
//						
//						
//						
////						double psnr = ProblemUtils.calcularPSNR(equalizer.getImagenOriginal(), imagenNueva);
//						if (sumaConAmbe >= bestSumaConAmbe){
//							bestSumaConAmbe = sumaConAmbe;
//							bestSumaConAmbeIndex = i;
//							bestImagenSumaConAmbe = imagenNueva;
//						}
//						if (ssim >= bestSSIM){
//							bestSSIM = ssim; 
//							bestImagenSSIM = imagenNueva;
//							bestSSIMIndex = i;
//						}
//						if (suma >= bestSuma){
//							bestSuma = suma; 
//							bestImagenSuma = imagenNueva;
//							bestSumaIndex = i;
//						}
//						
//						if (sumaContDiv2 >= bestSumaCons2){
//							bestSuma = sumaContDiv2; 
//							bestImagenSumaCons2 = imagenNueva;
//							bestSumaCons2Index = i;
//						}
//						if (bestImagenSumaConAmbe!=null){
//							GraphicsHelper.saveToFolder(ruta,
//								nombreImagen, formato,
//								equalizer.getImagenOriginal(), bestImagenSumaConAmbe,
//								 "BEST-SUMA-ESCA - " + bestSumaConAmbeIndex);
//						}
//						if (bestImagenSSIM!=null){
//							GraphicsHelper.saveToFolder(ruta,
//								nombreImagen, formato,
//								equalizer.getImagenOriginal(), bestImagenSSIM,
//								 "BEST-SSIM - " + bestSSIMIndex);
//						}
//						if (bestImagenSuma!=null){
//							GraphicsHelper.saveToFolder(ruta,
//								nombreImagen, formato,
//								equalizer.getImagenOriginal(), bestImagenSuma,
//								 "BEST-SUMA-ESC - " + bestSumaIndex);
//						}
//						if (bestImagenSumaCons2!=null){
//							GraphicsHelper.saveToFolder(ruta,
//								nombreImagen, formato,
//								equalizer.getImagenOriginal(), bestImagenSumaCons2,
//								 "BEST-SUMA-ES-CD2 - " + bestSumaCons2Index);
						} 
*/
	
				}
			}
//		---------------------------------------------------------	
//		---------------------MOHE D -----------------------------
//		---------------------------------------------------------	
		} else if (algorithm.equalsIgnoreCase("moheD")) {
			File theDir = new File(baseImagesPath +"/moheD/"+ args[2]);
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/moheD/"+ args[2]+"/0/" );
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/moheD/"+ args[2]+"/" + i +"/");
				i++;
			}

			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();
					MoheEqualizer equalizer = new MoheEqualizer(ruta,
							nombreImagen, formato, true);
					equalizer.setThresholderMethod(Method.valueOf(args[2]));
					equalizer.equalize();
					i = 0;
					for (Imagen imagenNueva : equalizer.getImagenesNuevas()) {
						i++;
						GraphicsHelper.saveToFolder(ruta,
								nombreImagen, formato,
								equalizer.getImagenOriginal(), imagenNueva,
								 ""+ i);

					}
				}

			}
//		------------------------------------------------
//		----------------ASP------------------------------	
//		-----------------------------------------------
		} else if (algorithm.equalsIgnoreCase("ASP")) {
			ASPApp app;
			File theDir = new File(baseImagesPath +"/ASP");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/ASP/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/ASP/" + i);
				i++;
			}

			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {

				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];

				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath() + "/"
							+ listOfimages[j].getName() + "/";
					File fichero = new File(ruta);
					fichero.mkdir();
					double[][] aspValues = new double[3][2];
					aspValues[0][0] = 0.001;
					aspValues[0][1] = 0.9611;
					aspValues[1][0] = 0.001;
					aspValues[1][1] = 1.0381;
					aspValues[2][0] = 0.0009;
					aspValues[2][1] = 1.1307;

					Double qualityIndex = 0D;
					int bestTitaGamaIndex = 0;
					for (int i1 = 0; i1 < 3; i1++) {
						app = new ASPApp(nombreImagen, formato);
						app.getAsp().equalize(aspValues[i1][0],
								aspValues[i1][1]);
						ByteProcessor imagenOriginalJ = new ByteProcessor(app
								.getAsp().getImagenOriginal().getWidth(), app
								.getAsp().getImagenOriginal().getHeight(), app
								.getAsp().getImagenOriginal().getSrcData());
						ByteProcessor imagenNuevaJ = new ByteProcessor(app
								.getAsp().getImagenOriginal().getWidth(), app
								.getAsp().getImagenOriginal().getHeight(), app
								.getAsp().getImagenNueva().getSrcData());
						Double nqi = ProblemUtils.calcularSSIM(imagenOriginalJ,
								imagenNuevaJ);
						if (nqi > qualityIndex) {
							qualityIndex = nqi;
							bestTitaGamaIndex = i1;
						}
					}
					ASPApp newapp = new ASPApp(ruta, nombreImagen, formato);
					newapp.getAsp().equalize(aspValues[bestTitaGamaIndex][0],
							aspValues[bestTitaGamaIndex][1]);
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato, newapp.getAsp()
									.getImagenOriginal(), newapp.getAsp()
									.getImagenNueva(), "ASP");

				}

			}
//		---------------------------------------------
//		----------------HPSO-------------------------
//		---------------------------------------------	
		} else if (algorithm.equalsIgnoreCase("HPSO")) {
					File theDir = new File(baseImagesPath +"/HPSO");
					theDir.mkdirs();
					theDir = new File(baseImagesPath +"/HPSO/0");
					int i = 1;
					while (!theDir.mkdir()) {
						theDir = new File(baseImagesPath +"/HPSO/" + i);
						i++;
					}

					File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
					File[] listOfimages = baseImagesFolder.listFiles();

					for (int j = 0; j < listOfimages.length; j++) {
						
						String[] fileData = listOfimages[j].getName().split("\\.");
						String nombreImagen = fileData[0];
						String formato = fileData[1];
						
						if (listOfimages[j].isFile()) {
							String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
							File fichero = new File(ruta);
							fichero.mkdir();
							HPSOEqualizer equalizer = new HPSOEqualizer(ruta,
									nombreImagen, formato);
							equalizer.equalize();
							i = 0;
							for (Imagen imagenNueva : equalizer.getImagenesNuevas()) {
								i++;
								GraphicsHelper.saveToFolder(ruta,
										nombreImagen, formato,
										equalizer.getImagenOriginal(), imagenNueva,
										 ""+ i);

							}
						}

					}
//		---------------------------------------			
//		------------------HISG------------------			
//		----------------------------------------			
		}else if (algorithm.equalsIgnoreCase("HSIG")) {
			File theDir = new File(baseImagesPath +"/hSig");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/hSig/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/hSig/" + i);
				i++;
			}

			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();
					SigmoidHistogramEqualizer equalizer = new SigmoidHistogramEqualizer(ruta,
							nombreImagen, formato);
					equalizer.equalize();
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							equalizer.getImagenOriginal(), equalizer.getImagenNueva(),
							 "HSig");

				}

			}
//		---------------------------------------------
//		----------------------HE---------------------	
//		---------------------------------------------	
		}else if (algorithm.equalsIgnoreCase("HE")) {
			File theDir = new File(baseImagesPath +"/HE");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/HE/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/HE/" + i);
				i++;
			}

			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 1; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();
					BaseHistogramEqualizer equalizer = new BaseHistogramEqualizer(ruta,
							nombreImagen, formato);
					equalizer.equalize();
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							equalizer.getImagenOriginal(), equalizer.getImagenNueva(),
							 "HE");

				}

			}
//			-------------CLAHE?-----------------------
			
		}else if (algorithm.equalsIgnoreCase("CLAHER")) {
			File theDir = new File(baseImagesPath +"/CLAHER");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/CLAHER/0");
			theDir.mkdir();
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			File claheImagesFolder = new File(baseImagesPath +"/claheRImages/");
			File[] listclaheOfimages = claheImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					for (int i = 0; i< listclaheOfimages.length; i++){
						String[] claheFileData = listclaheOfimages[i].getName().split("\\.");
						String claheNombreImagen = claheFileData[0];
						String formatoImagenClahe = claheFileData[1];
						if ((claheNombreImagen.split("clahe")[0]).equals(nombreImagen)){
							String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
							File fichero = new File(ruta);
							fichero.mkdir();
							Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/" + nombreImagen + "." + formato);
							Imagen imagenClahe = new Imagen(baseImagesPath +"/claheRImages/" + claheNombreImagen + "." + formatoImagenClahe);
							GraphicsHelper.saveToFolder(ruta,
									nombreImagen, formato,
									imagenOriginal, imagenClahe,
									"CLAHER");
							try{
								BufferedWriter bw = new BufferedWriter(new FileWriter(ruta + "/CLAHERFUN.txt" , false));
								double entropy = ProblemUtils.calcularEntropia(imagenClahe.getHistograma(),imagenClahe.getCantPixeles());
								double ssim = ProblemUtils.calcularSSIM(imagenOriginal,imagenClahe);
								bw.append(-entropy + " " + -ssim );
								bw.close();
							}catch(Exception e){
								System.out.println(e.getStackTrace());
							}
							break;
						}
					}

				}

			}
		}else if (algorithm.equalsIgnoreCase("CLAHELicen")) {
			File theDir = new File(baseImagesPath +"/CLAHELicen");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/CLAHELicen/0");
			theDir.mkdir();
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			File claheImagesFolder = new File(baseImagesPath +"/claheLicenImages/");
			File[] listclaheOfimages = claheImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					for (int i = 0; i< listclaheOfimages.length; i++){
						String[] claheFileData = listclaheOfimages[i].getName().split("\\.");
						String claheNombreImagen = claheFileData[0];
						String formatoImagenClahe = claheFileData[1];
						if ((claheNombreImagen.split("clahe")[0]).equals(nombreImagen)){
							String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
							File fichero = new File(ruta);
							fichero.mkdir();
							Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/" + nombreImagen + "." + formato);
							Imagen imagenClahe = new Imagen(baseImagesPath +"/claheLicenImages/" + claheNombreImagen + "." + formatoImagenClahe);
							GraphicsHelper.saveToFolder(ruta,
									nombreImagen, formato,
									imagenOriginal, imagenClahe,
									"CLAHEL");
							try{
								BufferedWriter bw = new BufferedWriter(new FileWriter(ruta + "/CLAHELFUN.txt" , false));
								double entropy = ProblemUtils.calcularEntropia(imagenClahe.getHistograma(),imagenClahe.getCantPixeles());
								double ssim = ProblemUtils.calcularSSIM(imagenOriginal,imagenClahe);
								bw.append(-entropy + " " + -ssim );
								bw.close();
							}catch(Exception e){
								System.out.println(e.getStackTrace());
							}
							
							break;
						}
					}

				}

			}
		}else if (algorithm.equalsIgnoreCase("ORI")) {
			File theDir = new File(baseImagesPath +"/ORI");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/ORI/0");
			theDir.mkdir();
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();
					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/" + nombreImagen + "." + formato);
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, imagenOriginal,
							"ORIGINAL");
					try{
						BufferedWriter bw = new BufferedWriter(new FileWriter(ruta + "/ORIFUN.txt" , false));
						double entropy = ProblemUtils.calcularEntropia(imagenOriginal.getHistograma(),imagenOriginal.getCantPixeles());
						double ssim = 1D;
						bw.append(-entropy + " " + -ssim );
						bw.close();
					}catch(Exception e){
						System.out.println(e.getStackTrace());
					}

				}

			}
		} else if (algorithm.equalsIgnoreCase("moheN")) {
			File theDir = new File(baseImagesPath +"/propuesta");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/propuesta/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/propuesta/" + i);
				i++;
			}
			
			Double alfa1 = new Double (args[2]);
			Double alfa2 = new Double (args[3]);
			Double alfa3 = new Double (args[4]);

			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();
					MoheEqualizer equalizer = new MoheEqualizer(ruta,
							nombreImagen, formato, true , alfa1, alfa2, alfa3);
					equalizer.equalize();
					i = 0;
					for (Imagen imagenNueva : equalizer.getImagenesNuevas()) {
						i++;
						GraphicsHelper.saveToFolder(ruta,
								nombreImagen, formato,
								equalizer.getImagenOriginal(), imagenNueva,
								 ""+ i);

					}
				}

			}

		} else if (algorithm.equalsIgnoreCase("BBHE")) {
			File theDir = new File(baseImagesPath +"/BBHE");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/BBHE/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/BBHE/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionBBHE(ip, imagenOriginal.getHistograma());
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "BBHE");

				}

			}

		} else if (algorithm.equalsIgnoreCase("DSIHE")) {
			File theDir = new File(baseImagesPath +"/DSIHE");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/DSIHE/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/DSIHE/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionDSIHE(ip, imagenOriginal.getHistograma());
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "DSIHE");

				}

			}

		} else if (algorithm.equalsIgnoreCase("MMBEBHE")) {
			File theDir = new File(baseImagesPath +"/MMBEBHE");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/MMBEBHE/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/MMBEBHE/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionMMBEBHE(ip, imagenOriginal.getHistograma());
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "MMBEBHE");

				}

			}

		} else if (algorithm.equalsIgnoreCase("BHEPL")) {
			File theDir = new File(baseImagesPath +"/BHEPL");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/BHEPL/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/BHEPL/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionBHEPL(ip, imagenOriginal.getHistograma(),true);
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "BHEPL");

				}

			}

		} else if (algorithm.equalsIgnoreCase("Infrarojo2PL")) {
			File theDir = new File(baseImagesPath +"/Infrarojo2PL");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/Infrarojo2PL/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/Infrarojo2PL/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionInfrarojo2PL(ip, imagenOriginal.getHistograma());
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "Infrarojo2PL");

				}

			}

		} else if (algorithm.equalsIgnoreCase("BHE3PL")) {
			File theDir = new File(baseImagesPath +"/BHE3PL");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/BHE3PL/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/BHE3PL/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionBHE3PL(ip, imagenOriginal.getHistograma());
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "BHE3PL");

				}

			}

		} else if (algorithm.equalsIgnoreCase("BHE2PL")) {
			File theDir = new File(baseImagesPath +"/BHE2PL");
			theDir.mkdirs();
			theDir = new File(baseImagesPath +"/BHE2PL/0");
			int i = 1;
			while (!theDir.mkdir()) {
				theDir = new File(baseImagesPath +"/BHE2PL/" + i);
				i++;
			}
			
			
			File baseImagesFolder = new File(baseImagesPath +"/baseImages/");
			File[] listOfimages = baseImagesFolder.listFiles();

			for (int j = 0; j < listOfimages.length; j++) {
				
				String[] fileData = listOfimages[j].getName().split("\\.");
				String nombreImagen = fileData[0];
				String formato = fileData[1];
				
				if (listOfimages[j].isFile()) {
					String ruta = theDir.getPath()+"/"+listOfimages[j].getName()+"/";
					File fichero = new File(ruta);
					fichero.mkdir();

					Imagen imagenOriginal = new Imagen(baseImagesPath +"/baseImages/"+ nombreImagen + "." + formato);
					ImageProcessor ip =  (ByteProcessor)new ByteProcessor(imagenOriginal.srcImage).duplicate();
					MetodoEcualizacion.ecualizacionBHE2PL(ip, imagenOriginal.getHistograma());
					GraphicsHelper.saveToFolder(ruta,
							nombreImagen, formato,
							imagenOriginal, new Imagen(ip.getBufferedImage()),
							 "BHE2PL");

				}

			}

		}else  {
			System.out.println("ALGORITHM NOT CONFIGURED OR NOT IMPLEMENTED");
		}

	}

	private  List<Imagen> getImangenesDominates(Imagen imagenOriginal, List<Imagen> imagenesNuevas) {
		return null;
		
	}
	
	
	
}

// public List<String> getAlgorithmsClassNames() {
// List<String> algorithmsToRun = new ArrayList<String>();
// try{
// FileInputStream fis = new FileInputStream("rsc/files/algorithms.txt");
// InputStreamReader isr = new InputStreamReader(fis);
// BufferedReader br = new BufferedReader(isr);
//
// String aux = br.readLine();
// while (aux != null) {
// algorithmsToRun.add(aux);
// aux = br.readLine();
// }
// br.close();
// }catch(Exception e){
// System.out.println(e.getStackTrace());
// }
// return algorithmsToRun;
//
// }

