package hpso;

import java.io.FileNotFoundException;
import java.util.Iterator;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.smpso.SMPSO;

public class HPSO extends SMPSO{
	
	private SolutionSet initialSolutionSet;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HPSO(Problem problem, String trueParetoFront) throws FileNotFoundException {
		super(problem, trueParetoFront);
//		jmetal.qualityIndicator.util.MetricsUtil mu = new jmetal.qualityIndicator.util.MetricsUtil();
//		initialSolutionSet = mu.readNonDominatedSolutionSet(trueParetoFront);
	}
	
	public HPSO(Problem problem) {
		super(problem);
	}
	
	public HPSO(Problem problem, SolutionSet initialSolutionSet) {
		super(problem);
		this.initialSolutionSet=initialSolutionSet;
	}
	
	@Override
	public void initParams() {
		super.initParams();
		if (initialSolutionSet != null){
			Iterator<Solution> iterator = initialSolutionSet.iterator();
			while (iterator.hasNext()) {
				Solution sol = iterator.next();
				particles_.add(sol);
			}
		}
	}

}
