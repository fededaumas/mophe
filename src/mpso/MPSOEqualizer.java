package mpso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import ij.plugin.histogram2.HistogramMatcher;
import ij.plugin.histogram2.Util;
import ij.process.ByteProcessor;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.encodings.variable.Real;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import utils.GraphicsHelper;
import utils.Imagen;
import utils.ProblemUtils;

public class MPSOEqualizer extends Problem{
	
	Imagen imagenOriginal;
	List<Imagen> imagenesNuevas = new ArrayList<Imagen>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static int cantidadParticulas;
	public static int tamanhoArchivo;
	public static int cantidadIteraciones;
	public static String nombreImagen;
	public static String formatoImagen;

	private static Logger logger = Logger.getLogger(MPSOEqualizer.class);

	static {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("rsc/files/hpso.properties");

			// load a properties file
			prop.load(input);

			// get the property value
			cantidadParticulas = Integer.valueOf(prop
					.getProperty("algoritmo.cantidadparticulas"));
			tamanhoArchivo = Integer.valueOf(prop
					.getProperty("algoritmo.tamanhoArchivo"));
			cantidadIteraciones = Integer.valueOf(prop
					.getProperty("algoritmo.cantidaditeraciones"));
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	
	private void setPSOVariables() {
		numberOfVariables_ = 1;
		numberOfObjectives_ = 2;
		problemName_ = "mpsoProblema";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		
		for (int i=0; i< numberOfVariables_; i++){
			lowerLimit_[i] = 0.5;
			upperLimit_[i] = 1.5;
		}
		
		solutionType_ = new RealSolutionType(this);

	}
	
	public MPSOEqualizer(String imageFileName, String imageFormat) {
		String imagen = imageFileName != null ? imageFileName : "lena_dark";
		String formato = imageFormat != null ? imageFormat : "png";
		String ruta = "rsc/images/baseImages/";
		imagenOriginal = new Imagen(ruta + imagen + "." + formato);
		setPSOVariables();
	}

	public MPSOEqualizer(Imagen imagenOriginal) {

		this.imagenOriginal = imagenOriginal;
		setPSOVariables();
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] variables = solution.getDecisionVariables();
		
		double gamma = variables[0].getValue();
		ByteProcessor imagenOriginalJ = new ByteProcessor(
				imagenOriginal.getSrcImage());
		ByteProcessor imagenNuevaJ = (ByteProcessor) imagenOriginalJ.duplicate();
		imagenNuevaJ.resetRoi();
		int[] hist = imagenNuevaJ.getHistogram();

		double cdf[] = Util.Cdf(hist);
		double df[] = new double[256];
		
		for (int i =0; i<256; i++){
//			cdf[i] = Math.pow(cdf[i], gamma) * 255;
			df[i] = new Double (new Double((i+1))/256D);
//			df[i] = (int)(cdf[i]);
		}
		HistogramMatcher matcher = new HistogramMatcher();
		int [] LUT = matcher.matchHistograms(cdf, df);
		imagenNuevaJ.applyTable(LUT);
		Imagen imagenNueva = new Imagen(imagenNuevaJ.getBufferedImage());
		
		
		
		solution.setObjective(0, -ProblemUtils.calcularEntropia(imagenNueva.getHistograma(), imagenNueva.getCantPixeles()));
		solution.setObjective(1, -ProblemUtils.calcularSSIM(imagenOriginalJ,imagenNuevaJ));
		
		
		
		
		
	}
	
	public static void main(String[] args) {
		String ruta = "rsc/images/baseImages/";
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("rsc/files/image_file.properties");

			// load a properties file
			prop.load(input);

			nombreImagen = prop.getProperty("nombreimagen");
			formatoImagen = prop.getProperty("formato");
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		MPSOEqualizer equalizer = new MPSOEqualizer(nombreImagen , formatoImagen);
		equalizer.equalize();
		int i =0;
		for (Imagen imagenNueva : equalizer.getImagenesNuevas()){
			i++;
//			if (i == 1 || i== equalizer.getImagenesNuevas().size() )
			GraphicsHelper.imprimirFrame(ruta, nombreImagen,
					equalizer.getImagenOriginal(),
					imagenNueva, "HSPO" + i);
			
		}
		
		
	}

	public void equalize() {
		try {
			
//			SolutionSet initialPareto = getFromParetoFront("rsc/files/INITIAL_PARETO.txt");
			MPSO pso = new MPSO(this);

			pso.setInputParameter("swarmSize", cantidadParticulas);
			pso.setInputParameter("archiveSize", tamanhoArchivo);
			pso.setInputParameter("maxIterations", cantidadIteraciones);
			
//			QualityIndicator indicators = new QualityIndicator(this, "rsc/files/MOHEFUN.txt");
//			pso.setInputParameter("indicators", indicators);

			HashMap parameters = new HashMap();// Operator parameters
			parameters.put("probability", 1.0 / (getNumberOfVariables()/36)); //stronger mutation
//			parameters.put("probability", 1.0 / getNumberOfVariables());// para
																		// mutacion
			parameters.put("distributionIndex", 20.0);// para mutacion

			Mutation mutacion;
				mutacion = MutationFactory.getMutationOperator(
						"PolynomialMutation", parameters);
			// operador de turbulencia
														// (mutacion)
			pso.addOperator("mutation", mutacion);

			long initTime = System.currentTimeMillis();
			SolutionSet poblacion = pso.execute();// ejecutar el pso
			long estimatedTime = System.currentTimeMillis() - initTime;
			logger.info("Tiempo total: " + estimatedTime + " ms");

			Iterator<Solution> iterator = poblacion.iterator();
			
			poblacion.printFeasibleFUN("rsc/files/MPSOFUN.txt");
			PrintWriter pw = new PrintWriter("rsc/files/MPSOVAR.txt");
			pw.close();
			while (iterator.hasNext()) {
				Solution sol = iterator.next();
				Variable[] variables = sol.getDecisionVariables();
				double gamma = variables[0].getValue();
				
				 
				ByteProcessor imagenOriginalJ = new ByteProcessor(
						imagenOriginal.getSrcImage());
				
				ByteProcessor imagenNuevaJ = (ByteProcessor) imagenOriginalJ.duplicate();
				imagenNuevaJ.resetRoi();
				int[] hist = imagenNuevaJ.getHistogram();
				
				
				double cdf[] = Util.Cdf(hist);
				double df[] = new double[256];
				
				for (int i =0; i<256; i++){
//					cdf[i] = Math.pow(cdf[i], gamma) * 255;
					df[i] = new Double (new Double((i+1))/256D);
				}
				HistogramMatcher matcher = new HistogramMatcher();
				int [] LUT = matcher.matchHistograms(cdf, df);
				imagenNuevaJ.applyTable(LUT);
				addFeasibleVARMatchedHistogram("rsc/files/MPSOVAR.txt", imagenNuevaJ.getHistogram());
				imagenesNuevas.add(new Imagen(imagenNuevaJ.getBufferedImage()));
				
			}
		}catch(Exception e){
			logger.error("Error", e);
		}

		
	}

	
	private void addFeasibleVARMatchedHistogram(String path, int[] histogram) {
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter(path, true));

			int numberOfVariables = histogram.length;
			for (int j = 0; j < numberOfVariables; j++) {
				bw.append(histogram[j] + " ");
			}
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			Configuration.logger_.severe("Error acceding to the file");
			e.printStackTrace();
		}
	}
	
	public Imagen getImagenOriginal() {
		return imagenOriginal;
	}

	public void setImagenOriginal(Imagen imagenOriginal) {
		this.imagenOriginal = imagenOriginal;
	}

	public List<Imagen> getImagenesNuevas() {
		return imagenesNuevas;
	}

	public void setImagenesNuevas(List<Imagen> imagenesNuevas) {
		this.imagenesNuevas = imagenesNuevas;
	}

	private SolutionSet getFromParetoFront(String path){
		 try {
		      /* Open the file */
		      FileInputStream fis   = new FileInputStream(path)     ;
		      InputStreamReader isr = new InputStreamReader(fis)    ;
		      BufferedReader br      = new BufferedReader(isr)      ;
		      
		      SolutionSet solutionSet = new SolutionSet(cantidadParticulas);
		      
		      String aux = br.readLine();
		      while (aux!= null) {
		        StringTokenizer st = new StringTokenizer(aux);
		        int i = 0;
		        Solution solution = new Solution(this);
		        Variable[] variables = new Variable[256];
		        while (st.hasMoreTokens()) {
		          double value = new Double(st.nextToken());
		          if (i<2)solution.setObjective(i,value);
		          else{
		        	  Variable v= new Real(0, imagenOriginal.getMaxLevelValue());
		        	  v.setValue(value);
		        	  variables[i-2] = v;
		          }
		          i++;
		        }
		        solution.setDecisionVariables(variables);
		        solutionSet.add(solution);
		        aux = br.readLine();
		      }
		      br.close();
		      return solutionSet;
		    } catch (Exception e) {
		      System.out.println("jmetal.qualityIndicator.util.readNonDominatedSolutionSet: "+path);
		      e.printStackTrace();
		    }
		    return null;
	}
	
	
}
